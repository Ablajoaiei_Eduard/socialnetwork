package utils;

import org.mindrot.jbcrypt.BCrypt;

public class Crypter {
    public static String hash(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(10));
    }

    public static boolean verifyHash(String password, String hash) {
        if(password == null)
            return false;
        return BCrypt.checkpw(password, hash);
    }
}
