
import controller.LoginScreenController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.NetworkService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainFX extends Application {
    socialnetwork.service.NetworkService networkService;

    @Override
    public void start(Stage stage) throws IOException {
        try {
            socialnetwork.repository.database.UtilizatorDB utilizatorRepository =
                    new socialnetwork.repository.database.UtilizatorDB(new socialnetwork.domain.validators.UtilizatorValidator());
            socialnetwork.repository.Repository<socialnetwork.domain.Tuple<Long,Long>, socialnetwork.domain.Prietenie> prietenieRepository =
                    new socialnetwork.repository.database.PrietenieDB(new socialnetwork.domain.validators.PrietenieValidator());
            socialnetwork.service.NetworkService networkService = new NetworkService(utilizatorRepository, prietenieRepository);
            this.networkService = networkService;

            initView(stage);
            stage.show();
        }
        catch (Exception e){
            Alert msg = new Alert(Alert.AlertType.ERROR, e.getMessage());
            msg.show();
        }



    }

    public static void main(String[] args) {

        launch(args);
    }
    private void initView(Stage primaryStage) throws IOException {

        FXMLLoader messageLoader = new FXMLLoader();
        messageLoader.setLocation(getClass().getResource("loginScreen.fxml"));
        VBox messageTaskLayout = messageLoader.load();
        primaryStage.setScene(new Scene(messageTaskLayout));

        LoginScreenController loginScreenController = messageLoader.getController();
        loginScreenController.setService(this.networkService);
        Stage stage = (Stage) messageTaskLayout.getScene().getWindow();
        stage.getIcons().add(new Image("images/coffeelogo.png"));

    }
}