package socialnetwork.repository.database;

import repository.paging.Page;
import repository.paging.Pageable;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UtilizatorDB extends socialnetwork.repository.database.AbstractDBRepository<Long, Utilizator> {


    public UtilizatorDB(Validator<Utilizator> validator) throws ClassNotFoundException, SQLException {

            super(validator);




    }

    @Override
    protected void loadData() throws SQLException, ClassNotFoundException {
        this.connect();
        Statement stmt = c.createStatement();

        ResultSet result = stmt.executeQuery("SELECT * FROM users");
        while(result.next()){
            Long ID = result.getLong("id");
            String lastName = result.getString("lastname");
            String firstName = result.getString("firstname");
            String mail = result.getString("emailadress");
            String hashedPassword = result.getString("hashedpassword");
            this.entities.put(ID, new Utilizator(mail, firstName, lastName, hashedPassword));
        }
        stmt.close();
        result.close();
        this.disconnect();
    }

    @Override
    public Iterable<Utilizator> findAll(){
        List<Utilizator> rez = new ArrayList<>();
        try{
            this.connect();
            Statement stmt = c.createStatement();

            ResultSet result = stmt.executeQuery("SELECT * FROM users");
            while(result.next()){
                Long ID = result.getLong("id");
                String lastName = result.getString("lastname");
                String firstName = result.getString("firstname");
                String mail = result.getString("emailadress");
                String hashedPassword = result.getString("hashedpassword");
                rez.add(new Utilizator(mail, firstName, lastName, hashedPassword));
            }
            stmt.close();
            result.close();
            this.disconnect();
        }catch(ClassNotFoundException | SQLException e){
            System.out.println(e.getMessage());
        }
        return rez;
    }


    @Override
    public Utilizator findOne(Long id){

        try{
            this.connect();
            Statement stmt = c.createStatement();

            ResultSet result = stmt.executeQuery("SELECT * FROM users where id = " + id);
            result.next();

                Long ID = result.getLong("id");
                String lastName = result.getString("lastname");
                String firstName = result.getString("firstname");
                String mail = result.getString("emailadress");
                String hashedPassword = result.getString("hashedpassword");


            stmt.close();
            result.close();
            this.disconnect();
            return new Utilizator(mail, firstName, lastName, hashedPassword);
        }catch(ClassNotFoundException | SQLException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    protected String getInsertSql(Utilizator u){

        return "INSERT INTO users(id, lastname, firstname, emailadress, hashedpassword)\n"+"VALUES("+
                u.getId()+
                ','+
                "'"+
                u.getLastName()+
                "'"+
                ','+
                "'"+
                u.getFirstName()+
                "'"+
                ','+
                "'"+
                u.getMail()+
                "'"+
                ","+
                "'"+
                u.getHashedPassword()+
                "'"+
                ");";

   }

   @Override
    protected String getDeleteSql(Long id){
        return "DELETE FROM users WHERE id =" + id;
   }

   public Page<Long> getFriendsOnPage(Long id, Pageable pageable){
        List<Long> utilizatori = new ArrayList<Long>();
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string =
                    "((select id1, id2, data, time\n from users inner join friendships on users.id = friendships.id1 or users.id = friendships.id2\n" +
                            "where users.id = " +
                            id+
                            "\n order by data, time)\n"+
                            "except\n"+
                            "(select id1, id2, data, time\n from users inner join friendships on users.id = friendships.id1 or users.id = friendships.id2\n" +
            "where users.id = " +
                    id+
                    "\n order by data, time\n"+
                            "fetch first " +
                            (pageable.getPageNumber() - 1) * pageable.getPageSize() +
                            " rows only))\norder by data,time\n" +
                            "limit " +
                            pageable.getPageSize();





            ResultSet result = stmt.executeQuery(string);
            while (result.next()) {
                Long id1 = result.getLong("id1");
                Long id2 = result.getLong("id2");
                if(id1.equals(id)){
                    utilizatori.add(id2);
                }
                else{
                    utilizatori.add(id1);
                }
            }
            stmt.close();
            result.close();
            this.disconnect();
        }
        catch(SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }

        return new Page<Long>(pageable, utilizatori.stream());
   }

   public Page<Utilizator> getUsersOnPageContainingString(String string, Pageable pageable) {
       List<Utilizator> rez = new ArrayList<>();
       try {
           this.connect();
           Statement stmt = c.createStatement();
           String condition = "'%" + string + "%'";
           String help = "((select *" +
                   " from users\n" +
                   "where concat(lastname,' ',firstname) like " +
                   condition +
                   " order by id)" +
                   " except" +
                   " (select *" +
                   " from users\n" +
                   "where concat(lastname,' ',firstname) like " +
                   condition +
                   " order by id" +
                   " fetch first " +
                   (pageable.getPageNumber() - 1) * pageable.getPageSize() +
                   " rows only))" +
                   " order by id" +
                   " limit " +
                   pageable.getPageSize();
           ResultSet result = stmt.executeQuery(help);

           while (result.next()) {
               Long ID = result.getLong("id");
               String lastName = result.getString("lastname");
               String firstName = result.getString("firstname");
               String mail = result.getString("emailadress");
               String hashedPassword = result.getString("hashedpassword");
               String aux = lastName + " " + firstName;

               rez.add(new Utilizator(mail, firstName, lastName, hashedPassword));
           }
           stmt.close();
           result.close();
           this.disconnect();
           return new Page<Utilizator>(pageable, rez.stream());
       } catch (ClassNotFoundException | SQLException e) {
           System.out.println(e.getMessage());
       }
       return null;
   }
}
