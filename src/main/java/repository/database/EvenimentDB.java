package repository.database;

import domain.Eveniment;
import repository.paging.Page;
import repository.paging.Pageable;
import repository.paging.PagingRepository;
import socialnetwork.domain.validators.Validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EvenimentDB extends socialnetwork.repository.database.AbstractDBRepository<Long, Eveniment>
        implements PagingRepository<Long, Eveniment> {
    public EvenimentDB(Validator<Eveniment> validator) throws ClassNotFoundException, SQLException {
        super(validator);
    }

    @Override
    protected void loadData() throws ClassNotFoundException, SQLException {

    }

    @Override
    protected String getInsertSql(Eveniment entity) throws SQLException, ClassNotFoundException {
        String string = "insert into events(publisher, name, data, id)\n"+
                "Values("+
                entity.getPublisher()+
                ","+
                "'"+
                entity.getName()+
                "'"+
                ","+
                "'"+
                entity.getDate()+
                "'"+
                ","+
                entity.getId()+
                ");";

        return string;
    }

    @Override
    protected String getDeleteSql(Long aLong) {
        return "DELETE FROM events WHERE id =" + aLong;
    }

    @Override
    public Eveniment save(Eveniment entity) throws Exception {
        Eveniment aux = super.save(entity);
        this.connect();
        Statement stmt = c.createStatement();
        for(Long id: entity.getSubscribers())
            stmt.executeUpdate("INSERT INTO eventsusers(eventid, userid)\n" +
                    "VALUES("+
                    entity.getId()+
                    ","+
                    id+
                    ")");
        stmt.close();
        c.commit();
        this.disconnect();
        return entity;
    }


    @Override
    public Page<Eveniment> getAll(Pageable pageable)  {

        List<Eveniment> l = new ArrayList<>();
        try {
            this.connect();
            Statement stmt = c.createStatement();


            ResultSet result1 = stmt.executeQuery(
                    "((select * from events\n" +
                            "order by data desc)\n" +
                            "except\n" +
                            "(select * from events\n" +
                            "order by data desc\n" +
                            "fetch first "+
                            (pageable.getPageNumber()-1)*pageable.getPageSize()+
                            " rows only))\n" +
                            "limit "+
                            pageable.getPageSize()
            );
            while (result1.next()) {

                Long publisher = result1.getLong("publisher");
                LocalDate date = result1.getDate("data").toLocalDate();
                String name = result1.getString("name");
                Long id = result1.getLong("id");


                Eveniment eveniment = new Eveniment(id, publisher, name, date);

                Statement stmt2 = c.createStatement();
                ResultSet result2 = stmt2.executeQuery(
                        "SELECT userid FROM eventsusers WHERE eventid=" +
                                eveniment.getId());
                List<Long> list = new ArrayList<Long>();
                while (result2.next())
                    list.add(result2.getLong("userid"));
                eveniment.setSubscribers(list);

                stmt2.close();
                l.add(eveniment);
            }
            stmt.close();
            result1.close();
            this.disconnect();
            return new Page<Eveniment>(pageable, l.stream());
        }
        catch (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    public int getSize() {
        return 0;
    }


    public List<Eveniment> getAllForUser(Long userId){
        List<Eveniment> l = new ArrayList<>();
        try {
            this.connect();
            Statement stmt = c.createStatement();

            String help = "select * from events\n" +
                    "where publisher = " +
                    userId +
                    "\norder by data desc\n";
            ResultSet result1 = stmt.executeQuery(help);
            while (result1.next()) {

                Long publisher = result1.getLong("publisher");
                LocalDate date = result1.getDate("data").toLocalDate();

                String name = result1.getString("name");
                Long id = result1.getLong("id");


                Eveniment eveniment = new Eveniment(id, publisher, name, date);

                Statement stmt2 = c.createStatement();
                ResultSet result2 = stmt2.executeQuery(
                        "SELECT userid FROM eventsusers WHERE eventid=" +
                                eveniment.getId());
                List<Long> list = new ArrayList<Long>();
                while (result2.next())
                    list.add(result2.getLong("userid"));
                eveniment.setSubscribers(list);

                stmt2.close();
                l.add(eveniment);
            }
            stmt.close();
            result1.close();
            this.disconnect();
            return l;
        }
        catch (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    public Page<Eveniment> getAllForUser(Pageable pageable, Long userId)  {

        List<Eveniment> l = new ArrayList<>();
        try {
            this.connect();
            Statement stmt = c.createStatement();

            String help = "((select * from events\n" +
                    "where publisher = " +
                    userId +
                    "\norder by data desc)\n" +


                    "except\n" +
                    "(select * from events\n" +
                    "where publisher = " +
                    userId +
                    "\norder by data desc\n" +
                    "fetch first "+
                    (pageable.getPageNumber()-1)*pageable.getPageSize()+
                    " rows only))\n" +
                    "order by data desc\n"+
                    "limit "+
                    pageable.getPageSize();
            ResultSet result1 = stmt.executeQuery(help);
            while (result1.next()) {

                Long publisher = result1.getLong("publisher");
                LocalDate date = result1.getDate("data").toLocalDate();

                String name = result1.getString("name");
                Long id = result1.getLong("id");


                Eveniment eveniment = new Eveniment(id, publisher, name, date);

                Statement stmt2 = c.createStatement();
                ResultSet result2 = stmt2.executeQuery(
                        "SELECT userid FROM eventsusers WHERE eventid=" +
                                eveniment.getId());
                List<Long> list = new ArrayList<Long>();
                while (result2.next())
                    list.add(result2.getLong("userid"));
                eveniment.setSubscribers(list);

                stmt2.close();
                l.add(eveniment);
            }
            stmt.close();
            result1.close();
            this.disconnect();
            return new Page<Eveniment>(pageable, l.stream());
        }
        catch (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    public int getNumberOfEventsForUser(Long id){
        int rez = 0;
        try {
            this.connect();
            Statement stmt = c.createStatement();


            ResultSet result1 = stmt.executeQuery(
                    "select * from events\n" +
                            "where publisher = " +
                            id
            );
            while (result1.next()) {

                rez++;
            }
            stmt.close();
            result1.close();
            this.disconnect();
            return rez;
        }
        catch (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public void deletePastEvents(){
        try {
            this.connect();
            Statement stmt = c.createStatement();
            stmt.executeUpdate("delete from events where data < " + "'" + LocalDate.now() + "'") ;
            stmt.close();
            c.commit();
            this.disconnect();
        }
        catch  (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }

    }


    public boolean userGetsNotifiedByEvent(Eveniment eveniment, Long user){
        try {
            this.connect();
            Statement stmt = c.createStatement();


            ResultSet result1 = stmt.executeQuery(
                    "select getsnotified from eventsusers where userid = " +
                            user +
                            " AND eventid = "+
                            eveniment.getId()
            );
            result1.next();
            boolean rez = result1.getBoolean("getsnotified");


            stmt.close();
            result1.close();
            this.disconnect();
            return rez;
        }
        catch (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }

        return false;
    }

    public void registerUserToEvent(Eveniment eveniment, Long user){
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "insert into eventsusers(userid, eventid, getsnotified)\n"+
                    "values( "+ user + "," + eveniment.getId() + ", '0');";
            stmt.executeUpdate(string);
            stmt.close();
            c.commit();
            this.disconnect();
        }
        catch  (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
    }

    public boolean userIsRegisteredToEvent(Eveniment eveniment, Long user){
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "select * from eventsusers where eventid = "+ eveniment.getId() + " and userid = " + user;
            ResultSet result = stmt.executeQuery(string);
            boolean rez = true;
            if(!result.next())
                rez = false;
            stmt.close();
            c.commit();
            this.disconnect();
            return rez;
        }
        catch  (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public void subscribeUserToEvent(Eveniment eveniment, Long user){
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "update eventsusers\n"+
                            "set getsnotified = '1'\n"+
                            "where eventid = " +
                            eveniment.getId() +
                            "AND userid = " +
                            user;
            stmt.executeUpdate(string);
            stmt.close();
            c.commit();
            this.disconnect();
        }
        catch  (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
    }

    public void unsubscribeUserToEvent(Eveniment eveniment, Long user){
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "update eventsusers\n"+
                    "set getsnotified = '0'\n"+
                    "where eventid = " +
                    eveniment.getId() +
                    "AND userid = " +
                    user;
            stmt.executeUpdate(string);
            stmt.close();
            c.commit();
            this.disconnect();
        }
        catch  (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
    }

    public Eveniment getEvenimentById(Long id){
        try {
            this.connect();
            Statement stmt = c.createStatement();

            String help = "select * from events\n" +
                    "where id = " + id;
            ResultSet result1 = stmt.executeQuery(help);
            result1.next();
            Long publisher = result1.getLong("publisher");
            LocalDate date = result1.getDate("data").toLocalDate();
            String name = result1.getString("name");
            Eveniment eveniment = new Eveniment(id, publisher, name, date);

            Statement stmt2 = c.createStatement();
            ResultSet result2 = stmt2.executeQuery(
                    "SELECT userid FROM eventsusers WHERE eventid=" +
                            eveniment.getId());
            List<Long> list = new ArrayList<Long>();
            while (result2.next())
                list.add(result2.getLong("userid"));
            eveniment.setSubscribers(list);

            stmt2.close();

            stmt.close();
            result1.close();
            this.disconnect();
            return eveniment;
        }
        catch (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        return null;
    }


    public List<Eveniment> getSubscribedEventsForUser(Long user){
        List<Long> l = new ArrayList<>();
        try {
            this.connect();
            Statement stmt = c.createStatement();

            String help = "select eventid from eventsusers where userid = " +
                    user +
                    " and getsnotified = '1'";
            ResultSet result1 = stmt.executeQuery(help);
            while (result1.next()) {

                Long eventId = result1.getLong("eventid");
                l.add(eventId);
            }
            stmt.close();
            result1.close();
            this.disconnect();
            return l.stream()
                    .map(x->this.getEvenimentById(x))
                    .collect(Collectors.toList());
        }
        catch (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

}
