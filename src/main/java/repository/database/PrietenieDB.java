package socialnetwork.repository.database;

import socialnetwork.domain.Entity;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class PrietenieDB extends AbstractDBRepository<Tuple<Long,Long>, Prietenie> {

    public PrietenieDB(Validator<Prietenie> validator) throws SQLException, ClassNotFoundException {
        super(validator);
         loadData();

    }
    @Override
    protected void loadData() throws ClassNotFoundException, SQLException {
        this.connect();
        Statement stmt = c.createStatement();

        ResultSet result = stmt.executeQuery("SELECT * FROM friendships");
        while(result.next()){
            Long ID1 = result.getLong("id1");
            Long ID2 = result.getLong("id2");
            LocalDate date = result.getDate("data").toLocalDate();
            String time = result.getString("time");
            Prietenie p = new Prietenie(ID1, ID2);
            p.setDate(LocalDateTime.of(date, LocalTime.parse(time)));
            this.entities.put(p.getId(), p);
        }
        stmt.close();
        result.close();
        this.disconnect();
    }

    @Override
    protected String getInsertSql(Prietenie entity) throws SQLException, ClassNotFoundException {
        return "INSERT INTO friendships(id1, id2, data, time)\n"+"VALUES("+
                entity.getId().getLeft()+
                ','+
                entity.getId().getRight()+
                ","+
                "'"+
                entity.getDate().toLocalDate()+
                "'"+
                ","+
                "'"+
                entity.getDate().toLocalTime().toString()+
                "'"+
                ")";
    }

    @Override
    protected String getDeleteSql(Tuple<Long, Long> longLongTuple) {
        return "DELETE FROM friendships WHERE id1 ="+
                longLongTuple.getLeft()+
                " AND id2 ="+
                longLongTuple.getRight();
    }




}
