package socialnetwork.repository.database;

import repository.paging.Page;
import repository.paging.Pageable;
import repository.paging.PagingRepository;
import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.memory.InMemoryRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class MessageDB extends AbstractDBRepository<Long, Message> implements PagingRepository<Long, Message> {
    public MessageDB(Validator<Message> validator) throws ClassNotFoundException, SQLException {
        super(validator);
    }

    @Override
    protected void loadData() throws ClassNotFoundException, SQLException {
        this.connect();
        Statement stmt = c.createStatement();

        ResultSet result1 = stmt.executeQuery("SELECT * FROM mesaje");

        while(result1.next()){

            Long from = result1.getLong("source");
            String message = result1.getString("message");
            LocalDate date = result1.getDate("data").toLocalDate();
            String time = result1.getString("time");
            Long repliedMessage = result1.getLong("replied_msg");

            LocalDateTime dateTime = LocalDateTime.of(date,LocalTime.parse(time));
            LocalDateTime aux = LocalDateTime.now();
            Message message1;
            if(repliedMessage == 0)
                message1 = new Message(from,new ArrayList<Long>(),message,dateTime);
            else
                message1 = new Message(from, new ArrayList<Long>(),message,dateTime,repliedMessage);
            Statement stmt2 = c.createStatement();
            ResultSet result2 = stmt2.executeQuery(
                    "SELECT iddestinatar FROM utilizatormesaje WHERE idmesaj="+
                            message1.getId());
            List<Long> list = new ArrayList<Long>();
            while(result2.next())
                list.add(result2.getLong("iddestinatar"));
            message1.setTo(list);
            this.entities.put(message1.getId(),message1);
            stmt2.close();

        }
        stmt.close();
        result1.close();
        this.disconnect();
    }

    @Override
    protected String getInsertSql(Message entity) throws SQLException, ClassNotFoundException {
        LocalDateTime aux = LocalDateTime.now();
        return "INSERT INTO mesaje(id, source, message, data, time, replied_msg)\n"+"VALUES("+
                entity.getId()+
                ','+
                entity.getFrom()+
                ','+
                "'"+
                entity.getMessage()+
                "'"+
                ','+
                "'"+
                entity.getData().toLocalDate()+
                "'"+
                ","+
                "'"+
                entity.getData().toLocalTime().toString()+
                "'"+
                ","+
                entity.getRepliedMessage()+
                ");";

    }

    @Override
    protected String getDeleteSql(Long aLong) {
        return "DELETE FROM mesaje WHERE id =" + aLong;
    }

    @Override
    public Message save(Message entity) throws Exception {
        Message aux = super.save(entity);
        this.connect();
        Statement stmt = c.createStatement();
        for(Long id: entity.getTo())
            stmt.executeUpdate("INSERT INTO utilizatormesaje(idmesaj,iddestinatar)\n" +
                    "VALUES("+
                    entity.getId()+
                    ","+
                    id+
                    ")");
        stmt.close();
        c.commit();
        this.disconnect();
        return entity;
    }


    @Override
    public Page<Message> getAll(Pageable pageable)  {


        try {
            this.connect();
            Statement stmt = c.createStatement();
            //Page<Message> rez =  new Page<>();

            ResultSet result1 = stmt.executeQuery(
                    "((select * from mesaje\n" +
                            "order by data desc)\n" +
                            "except\n" +
                            "(select * from mesaje\n" +
                            "order by data desc\n" +
                            "fetch first "+
                            (pageable.getPageNumber()-1)*pageable.getPageSize()+
                            " rows only))\n" +
                            "limit "+
                            pageable.getPageSize()
                    );
            while (result1.next()) {

                Long from = result1.getLong("source");
                String message = result1.getString("message");
                LocalDate date = result1.getDate("data").toLocalDate();
                String time = result1.getString("time");
                Long repliedMessage = result1.getLong("replied_msg");

                LocalDateTime dateTime = LocalDateTime.of(date, LocalTime.parse(time));
                LocalDateTime aux = LocalDateTime.now();
                Message message1;
                if (repliedMessage == 0)
                    message1 = new Message(from, new ArrayList<Long>(), message, dateTime);
                else
                    message1 = new Message(from, new ArrayList<Long>(), message, dateTime, repliedMessage);
                Statement stmt2 = c.createStatement();
                ResultSet result2 = stmt2.executeQuery(
                        "SELECT iddestinatar FROM utilizatormesaje WHERE idmesaj=" +
                                message1.getId());
                List<Long> list = new ArrayList<Long>();
                while (result2.next())
                    list.add(result2.getLong("iddestinatar"));
                message1.setTo(list);
                this.entities.put(message1.getId(), message1);
                stmt2.close();

            }
            stmt.close();
            result1.close();
            this.disconnect();
        }
        catch (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public Message findOne(Long aLong) {
        try {
            this.connect();
            Statement stmt = c.createStatement();

            ResultSet result1 = stmt.executeQuery(
                    "select * from mesaje where id = " + aLong);
            result1.next();
            Long from = result1.getLong("source");
            String message = result1.getString("message");
            LocalDate date = result1.getDate("data").toLocalDate();
            String time = result1.getString("time");
            Long repliedMessage = result1.getLong("replied_msg");

            LocalDateTime dateTime = LocalDateTime.of(date, LocalTime.parse(time));
            LocalDateTime aux = LocalDateTime.now();
            Message message1;
            if (repliedMessage == 0)
                message1 = new Message(from, new ArrayList<Long>(), message, dateTime);
            else
                message1 = new Message(from, new ArrayList<Long>(), message, dateTime, repliedMessage);
            Statement stmt2 = c.createStatement();
            ResultSet result2 = stmt2.executeQuery(
                    "SELECT iddestinatar FROM utilizatormesaje WHERE idmesaj=" +
                            message1.getId());
            List<Long> list = new ArrayList<Long>();
            while (result2.next())
                list.add(result2.getLong("iddestinatar"));
            message1.setTo(list);

            stmt2.close();
            stmt.close();
            result1.close();
            this.disconnect();
            return message1;

        } catch (ClassNotFoundException | SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public List<Message> getConversation(Long id1, Long id2){
        List<Message> rez = new ArrayList<Message>();
        try {
            this.connect();
            Statement stmt = c.createStatement();


            ResultSet result1 = stmt.executeQuery(
                            "select *\n"+
                                "from mesaje inner join utilizatormesaje on mesaje.id = utilizatormesaje.idmesaj\n"+
                                "where source = "+
                                id1+
                                "and iddestinatar = "+
                                id2+
                                    "or (source = "+
                                    id2 +
                                    " and iddestinatar = "+
                                    id1 +
                                    ")\n"+
                                "order by data,time"
            );
            while (result1.next()) {

                Long from = result1.getLong("source");
                String message = result1.getString("message");
                LocalDate date = result1.getDate("data").toLocalDate();
                String time = result1.getString("time");
                Long repliedMessage = result1.getLong("replied_msg");

                LocalDateTime dateTime = LocalDateTime.of(date, LocalTime.parse(time));
                LocalDateTime aux = LocalDateTime.now();
                Message message1;
                if (repliedMessage == 0)
                    message1 = new Message(from, new ArrayList<Long>(), message, dateTime);
                else
                    message1 = new Message(from, new ArrayList<Long>(), message, dateTime, repliedMessage);
                Statement stmt2 = c.createStatement();
                ResultSet result2 = stmt2.executeQuery(
                        "SELECT iddestinatar FROM utilizatormesaje WHERE idmesaj=" +
                                message1.getId());
                List<Long> list = new ArrayList<Long>();
                while (result2.next())
                    list.add(result2.getLong("iddestinatar"));
                message1.setTo(list);
                rez.add(message1);
                stmt2.close();

            }
            stmt.close();
            result1.close();
            this.disconnect();
            return rez;
        }
        catch (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        return null;

    }

    public Page<Message> getConversation(Long id1, Long id2, Pageable pageable){

        List<Message> rez = new ArrayList<Message>();
        try {
            this.connect();
            Statement stmt = c.createStatement();

            String string =
                    "((select *\n"+
                    "from mesaje inner join utilizatormesaje on mesaje.id = utilizatormesaje.idmesaj\n"+
                    "where source = "+
                    id1+
                    "and iddestinatar = "+
                    id2+
                    " or (source = "+
                    id2 +
                    " and iddestinatar = "+
                    id1 +
                    ")\n"+
                    "order by data,time)\n"+
                    "except\n" +
                    "(select *\n"+
                    "from mesaje inner join utilizatormesaje on mesaje.id = utilizatormesaje.idmesaj\n"+
                    "where source = "+
                    id1+
                    "and iddestinatar = "+
                    id2+
                    "or (source = "+
                    id2 +
                    " and iddestinatar = "+
                    id1 +
                    ")\n"+
                    "order by data,time\n"+
                    "fetch first " +
                    (pageable.getPageNumber() - 1) * pageable.getPageSize() +
                    " rows only)\norder by data,time)\n" +
                    "limit " +
                    pageable.getPageSize();

            ResultSet result1 = stmt.executeQuery(string);

            while (result1.next()) {

                Long from = result1.getLong("source");
                String message = result1.getString("message");
                LocalDate date = result1.getDate("data").toLocalDate();
                String time = result1.getString("time");
                Long repliedMessage = result1.getLong("replied_msg");

                LocalDateTime dateTime = LocalDateTime.of(date, LocalTime.parse(time));
                LocalDateTime aux = LocalDateTime.now();
                Message message1;
                if (repliedMessage == 0)
                    message1 = new Message(from, new ArrayList<Long>(), message, dateTime);
                else
                    message1 = new Message(from, new ArrayList<Long>(), message, dateTime, repliedMessage);
                Statement stmt2 = c.createStatement();
                ResultSet result2 = stmt2.executeQuery(
                        "SELECT iddestinatar FROM utilizatormesaje WHERE idmesaj=" +
                                message1.getId());
                List<Long> list = new ArrayList<Long>();
                while (result2.next())
                    list.add(result2.getLong("iddestinatar"));
                message1.setTo(list);
                rez.add(message1);
                stmt2.close();

            }
            stmt.close();
            result1.close();
            this.disconnect();
            return new Page<Message>(pageable, rez.stream());
        }
        catch (SQLException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
        return new Page<Message>(pageable, rez.stream());

    }


}
