package socialnetwork.repository.database;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.memory.InMemoryRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;

public abstract class AbstractDBRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID,E> {
    public static final String username = "postgres";
    public static final String passwd= "eduard12";
    public Connection c;
    protected void connect() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        c = DriverManager
                .getConnection("jdbc:postgresql://localhost:5432/SocialNetwork",
                        username, passwd);
        c.setAutoCommit(false);
    }

    protected void disconnect() throws SQLException {
        c.close();
    }
    public AbstractDBRepository(Validator<E> validator) throws ClassNotFoundException, SQLException {
        super(validator);

    }

    protected abstract void loadData() throws ClassNotFoundException, SQLException;

    @Override
    public E save(E entity) throws Exception {
        E aux = super.save(entity);
        if(aux != null)
            return null;
        String sql = this.getInsertSql(entity);
        this.connect();
        Statement stmt = c.createStatement();
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        this.disconnect();
        return entity;
    }



    @Override
    public E delete(ID id) throws SQLException, ClassNotFoundException {
        E aux = super.delete(id);
        if(aux == null)
            return null;
        this.connect();
        Statement stmt = c.createStatement();
        String sql = this.getDeleteSql(id);
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        this.disconnect();

        return aux;
    }
    protected abstract String getInsertSql(E entity) throws SQLException, ClassNotFoundException;

    protected abstract String getDeleteSql(ID id);
}
