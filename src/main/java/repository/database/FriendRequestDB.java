package socialnetwork.repository.database;

import repository.paging.Page;
import repository.paging.Pageable;
import repository.paging.PagingRepository;
import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.stream.Stream;

public class FriendRequestDB extends AbstractDBRepository<Tuple<Long,Long>, FriendRequest> implements PagingRepository<Tuple<Long,Long>, FriendRequest> {
    public FriendRequestDB(Validator<FriendRequest> validator) throws ClassNotFoundException, SQLException {
        super(validator);
    }

    @Override
    protected void loadData() throws ClassNotFoundException, SQLException {
        this.connect();
        Statement stmt = c.createStatement();

        ResultSet result = stmt.executeQuery("SELECT * FROM friendrequests");
        while(result.next()){
            Long source = result.getLong("source");
            Long destination = result.getLong("destination");
            LocalDate date = result.getDate("data").toLocalDate();
            String time = result.getString("time");
            FriendRequest friendRequest = new FriendRequest(source, destination);
            friendRequest.setDate(LocalDateTime.of(date, LocalTime.parse(time)));
            this.entities.put(friendRequest.getId(), friendRequest);
        }
        stmt.close();
        result.close();
        this.disconnect();
    }

    @Override
    protected String getInsertSql(FriendRequest entity) throws SQLException, ClassNotFoundException {
        return "INSERT INTO friendrequests(source, destination, data, time)\n"+"VALUES("+
                entity.getSource()+
                ","+
                entity.getDestination()+
                ","+
                "'"+
                entity.getDate().toLocalDate()+
                "'"+
                ","+
                "'"+
                entity.getDate().toLocalTime().toString()+
                "'"+
                ");";
    }

    @Override
    protected String getDeleteSql(Tuple<Long,Long> id) {
        return "DELETE FROM friendrequests WHERE source =" +
                id.getLeft() +
                " AND destination = " +
                id.getRight();
    }

    @Override
    public Page<FriendRequest> getAll(Pageable pageable) {
        ArrayList<FriendRequest> rez = new ArrayList();
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "((select * from friendrequests\n" +
                    "order by data,time desc)\n" +
                    "except\n" +
                    "(select * from friendrequests\n" +
                    "order by data,time desc\n" +
                    "fetch first " +
                    (pageable.getPageNumber() - 1) * pageable.getPageSize() +
                    " rows only))\norder by data,time desc\n" +
                    "limit " +
                    pageable.getPageSize();
            ResultSet result = stmt.executeQuery(string);
            while (result.next()) {
                Long source = result.getLong("source");
                Long destination = result.getLong("destination");
                LocalDate date = result.getDate("data").toLocalDate();
                String time = result.getString("time");
                FriendRequest friendRequest = new FriendRequest(source, destination);
                friendRequest.setDate(LocalDateTime.of(date, LocalTime.parse(time)));
                this.entities.put(friendRequest.getId(), friendRequest);
                rez.add(friendRequest);

            }
            stmt.close();
            result.close();
            this.disconnect();
        }
        catch(ClassNotFoundException | NullPointerException | SQLException e){
            System.out.println(e.getMessage());
        }
        return new Page<FriendRequest>(pageable, rez.stream());
    }

    @Override
    public FriendRequest findOne(Tuple<Long, Long> longLongTuple) {

        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "select * from friendrequests where source = " +
                    longLongTuple.getLeft()+
                    " AND destination = "+
                    longLongTuple.getRight();
            ResultSet result = stmt.executeQuery(string);

            if(result.next() == false)
                return null;

            Long source = result.getLong("source");
            Long destination = result.getLong("destination");
            LocalDate date = result.getDate("data").toLocalDate();
            String time = result.getString("time");
            FriendRequest friendRequest = new FriendRequest(source, destination);
            friendRequest.setDate(LocalDateTime.of(date, LocalTime.parse(time)));
            stmt.close();
            result.close();
            this.disconnect();
            return friendRequest;
        } catch (ClassNotFoundException | NullPointerException | SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }



    }

    public int getSize() {
        int rez = -1;
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "select count(*) from friendrequests";
            ResultSet result = stmt.executeQuery(string);
            result.next();
            rez = result.getInt("count");
            stmt.close();
            result.close();
            this.disconnect();
        } catch (ClassNotFoundException | NullPointerException | SQLException e) {
            System.out.println(e.getMessage());
        }
        return rez;
    }

    public Page<FriendRequest> getAllFriendRequestsFromUser(Long ID, Pageable pageable){

        ArrayList<FriendRequest> rez = new ArrayList();
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "((select * from friendrequests\n" +
                    "where source =  " +
                    ID +
                    "\n order by data,time desc)\n" +
                    "except\n" +
                    "(select * from friendrequests\n" +
                    "where source =  " +
                    ID +
                    "order by data,time desc\n" +
                    "fetch first " +
                    (pageable.getPageNumber() - 1) * pageable.getPageSize() +
                    " rows only))\norder by data,time desc\n" +
                    "limit " +
                    pageable.getPageSize();
            ResultSet result = stmt.executeQuery(string);
            while (result.next()) {
                Long source = result.getLong("source");
                Long destination = result.getLong("destination");
                LocalDate date = result.getDate("data").toLocalDate();
                String time = result.getString("time");
                FriendRequest friendRequest = new FriendRequest(source, destination);
                friendRequest.setDate(LocalDateTime.of(date, LocalTime.parse(time)));
                this.entities.put(friendRequest.getId(), friendRequest);
                rez.add(friendRequest);

            }
            stmt.close();
            result.close();
            this.disconnect();
        }
        catch(ClassNotFoundException | NullPointerException | SQLException e){
            System.out.println(e.getMessage());
        }
        return new Page<FriendRequest>(pageable, rez.stream());

    }

    public int getPendingSize(Long id){
        int rez = -1;
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "select count(*) from friendrequests where source = " + id;
            ResultSet result = stmt.executeQuery(string);
            result.next();
            rez = result.getInt("count");
            stmt.close();
            result.close();
            this.disconnect();
        } catch (ClassNotFoundException | NullPointerException | SQLException e) {
            System.out.println(e.getMessage());
        }
        return rez;

    }

    public Page<FriendRequest> getAllFriendRequestsForUser(Long ID, Pageable pageable){
        ArrayList<FriendRequest> rez = new ArrayList();
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "((select * from friendrequests\n" +
                    "where destination =  " +
                    ID +
                    "\n order by data,time desc)\n" +
                    "except\n" +
                    "(select * from friendrequests\n" +
                    "where destination =  " +
                    ID +
                    " order by data,time desc\n" +
                    "fetch first " +
                    (pageable.getPageNumber() - 1) * pageable.getPageSize() +
                    " rows only))\norder by data,time desc\n" +
                    "limit " +
                    pageable.getPageSize();
            ResultSet result = stmt.executeQuery(string);
            while (result.next()) {
                Long source = result.getLong("source");
                Long destination = result.getLong("destination");
                LocalDate date = result.getDate("data").toLocalDate();
                String time = result.getString("time");
                FriendRequest friendRequest = new FriendRequest(source, destination);
                friendRequest.setDate(LocalDateTime.of(date, LocalTime.parse(time)));
                this.entities.put(friendRequest.getId(), friendRequest);
                rez.add(friendRequest);

            }
            stmt.close();
            result.close();
            this.disconnect();
        }
        catch(ClassNotFoundException | NullPointerException | SQLException e){
            System.out.println(e.getMessage());
        }
        return new Page<FriendRequest>(pageable, rez.stream());

    }

    public int getReceivedSize(Long id){
        int rez = -1;
        try {
            this.connect();
            Statement stmt = c.createStatement();
            String string = "select count(*) from friendrequests where destination = " + id;
            ResultSet result = stmt.executeQuery(string);
            result.next();
            rez = result.getInt("count");
            stmt.close();
            result.close();
            this.disconnect();
        } catch (ClassNotFoundException | NullPointerException | SQLException e) {
            System.out.println(e.getMessage());
        }
        return rez;

    }

}
