package repository.paging;

import java.util.stream.Stream;

public class Page<T> {
    private Pageable pageable;
    private Stream<T> content;

    public Page(Pageable pageable, Stream<T> content) {
        this.pageable = pageable;
        this.content = content;
    }

    public Pageable getPageable() {
        return this.pageable;
    }

    public Pageable nextPageable() {
        return new Pageable(this.pageable.getPageNumber() + 1, this.pageable.getPageSize());
    }

    public Stream<T> getContent() {
        return this.content;
    }
}
