package repository.paging;

import com.sun.org.apache.bcel.internal.util.Repository;

public interface PagingRepository<ID ,
        E extends socialnetwork.domain.Entity<ID>>
    extends socialnetwork.repository.Repository<ID ,
                                                E >  {
    Page<E> getAll(Pageable pageable);

    int getSize();

    //E findOne(ID id);
}
