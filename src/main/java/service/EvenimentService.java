package service;

import domain.Eveniment;
import repository.database.EvenimentDB;
import repository.paging.Page;
import repository.paging.Pageable;
import sun.nio.ch.Util;

import java.util.List;
import java.util.stream.Collectors;

public class EvenimentService {
    EvenimentDB evenimentRepository;

    public EvenimentService(EvenimentDB evenimentRepository) {
        this.evenimentRepository = evenimentRepository;
        evenimentRepository.deletePastEvents();
    }

    int size = 4;
    int page = 1;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Eveniment> getCurrentEvenimentPageForUser(Long user){
        return evenimentRepository.getAllForUser(new Pageable(page, size), user).getContent().collect(Collectors.toList());
    }

    public void goNextPage(){
        page++;
    }

    public void goPreviousPage(){
        page--;
    }

    public void addEveniment(Eveniment eveniment){
        try {
            evenimentRepository.save(eveniment);
            registerUserToEvent(eveniment, eveniment.getPublisher());
            subscribeUserToEvent(eveniment.getPublisher(), eveniment);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public int getMaxPage(Long user){
        int aux = evenimentRepository.getNumberOfEventsForUser(user);
        if(aux % size == 0)
            return aux/size;
        return aux/size + 1;

    }

    public void registerUserToEvent(Eveniment eveniment, Long user){
        evenimentRepository.registerUserToEvent(eveniment, user);
    }


    public boolean userGetsNotifiedByEvent(Eveniment eveniment, Long user){
        return this.evenimentRepository.userGetsNotifiedByEvent(eveniment, user);
    }

    public void subscribeUserToEvent(Long user, Eveniment eveniment){
        evenimentRepository.subscribeUserToEvent(eveniment, user);
    }

    public void unsubscribeUserToEvent(Long user, Eveniment eveniment){
        evenimentRepository.unsubscribeUserToEvent(eveniment, user);
    }

    public boolean userIsRegisteredToEvent(Long user, Eveniment eveniment){
        return evenimentRepository.userIsRegisteredToEvent(eveniment, user);
    }

    public Eveniment getEventById(Long id){
        return evenimentRepository.getEvenimentById(id);
    }

    public List<Eveniment> getSubscribedEventsForUser(socialnetwork.domain.Utilizator utilizator){
        return evenimentRepository.getSubscribedEventsForUser(utilizator.getId());
    }

}
