package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

import java.util.*;

public class CommunityService {
    private Repository<Long, Utilizator> repoUtilizatori;
    private Repository<Tuple<Long,Long>, Prietenie> repoPrietenii;


    public CommunityService(Repository<Long, Utilizator> repo, Repository<Tuple<Long,Long>, Prietenie> repo_p) {
        this.repoUtilizatori = repo;
        this.repoPrietenii = repo_p;
    }

    /**
     *
     * @return the number of communities in the network
     */
    public Integer communitiesNumber(){
        HashMap<Long, Boolean> colorare = new HashMap<Long, Boolean>();
        for(Utilizator u: this.repoUtilizatori.findAll()){
            colorare.put(u.getId(), false);
        }
        Integer contor = 0;
        while(colorare.containsValue(false)){
            contor++;
            Long key = -1L;
            for(Map.Entry<Long, Boolean> e: colorare.entrySet())
                if(e.getValue().equals(false)) {
                    key = e.getKey();
                    e.setValue(true);
                    break;
                }

            this.bfs(colorare, key);
        }
        return contor;
    }


    /**
     *
     * @return the largest community
     */
    public List<Utilizator> largestCommunity(){
        HashMap<Long, Boolean> colorare = new HashMap<Long, Boolean>();
        for(Utilizator u: this.repoUtilizatori.findAll()){
            colorare.put(u.getId(), false);
        }
        Long cheiaComunitatiiMaxime = null;
        Integer marimeActuala = 0;
        for(Utilizator u:this.repoUtilizatori.findAll()) {
            for(Utilizator u2: this.repoUtilizatori.findAll()){
                colorare.compute(u2.getId(), (k,v)->v=false);
                Integer marimeComunitate = this.bfs(colorare, u.getId());
                if(marimeComunitate > marimeActuala){
                    marimeActuala = marimeComunitate;
                    cheiaComunitatiiMaxime = u.getId();
                }
            }

        }
        return this.communityStartingAt(cheiaComunitatiiMaxime);



    }



    /**
     *
     * @param key - The ID of an user
     * @return the community containing that user
     */
    protected List<Utilizator> communityStartingAt(Long key){
        List<Utilizator> rez = new ArrayList<>();
        Queue<Long> utilizatori = new PriorityQueue<Long>();
        HashMap<Long, Boolean> colorare = new HashMap<>();
        for(Utilizator u: this.repoUtilizatori.findAll()){
            colorare.put(u.getId(), false);
        }
        colorare.compute(key, (k,v)->v=true);

        utilizatori.add(key);
        rez.add(this.repoUtilizatori.findOne(key));
        while(!utilizatori.isEmpty()){
            Long utilizator = utilizatori.remove();
            for(Prietenie prietenie: this.repoPrietenii.findAll()){
                Long prieten;
                if(prietenie.getId().getLeft().equals(utilizator))
                    prieten = prietenie.getId().getRight();
                else if(prietenie.getId().getRight().equals(utilizator))
                    prieten = prietenie.getId().getLeft();
                else
                    prieten = null;
                if(prieten != null)
                    if(colorare.get(prieten).equals(false)){
                        rez.add(this.repoUtilizatori.findOne(prieten));
                        colorare.compute(prieten, (k,v)->v=true);
                        utilizatori.add(prieten);
                    }
            }
        }
        return rez;
    }


    protected Integer bfs(HashMap<Long, Boolean> colorare, Long first){
        Queue<Long> utilizatori = new PriorityQueue<Long>();
        HashMap<Long, Integer> distanta = new HashMap<>();
        distanta.put(first, 0);

        utilizatori.add(first);
        while(!utilizatori.isEmpty()){
            Long utilizator = utilizatori.remove();
            for(Prietenie prietenie: this.repoPrietenii.findAll()){
                Long prieten;
                if(prietenie.getId().getLeft().equals(utilizator))
                    prieten = prietenie.getId().getRight();
                else if(prietenie.getId().getRight().equals(utilizator))
                    prieten = prietenie.getId().getLeft();
                else
                    prieten = null;
                if(prieten != null)
                    if(colorare.get(prieten).equals(false)){
                        distanta.put(prieten, distanta.get(utilizator) + 1);
                        colorare.compute(prieten, (k,v)->v=true);
                        utilizatori.add(prieten);
                    }
            }
        }
        return Collections.max(distanta.values());
    }


}
