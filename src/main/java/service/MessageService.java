package socialnetwork.service;

import domain.FriendNoMessages;
import observer.Observable;
import observer.Observer;
import repository.paging.Page;
import repository.paging.Pageable;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MessageService implements Observable {
    private socialnetwork.repository.database.MessageDB repoMessage;
    Utilizator currentUser;
    List<Observer> observers = new ArrayList<>();

    public void setCurrentUser(Utilizator currentUser) {
        this.currentUser = currentUser;
    }

    public MessageService(socialnetwork.repository.database.MessageDB repo){
        this.repoMessage = repo;


    }

    public List<Message> conversationWith(Utilizator u){
        return repoMessage.getConversation(currentUser.getId(), u.getId());

    }

    public List<Message> conversationWithBetweenDates(Utilizator u, LocalDate start, LocalDate end){
        LocalDateTime start1 = LocalDateTime.of(start, LocalTime.of(10,10));
        LocalDateTime end1 = LocalDateTime.of(end, LocalTime.of(10,10));
        return repoMessage
                .getConversation(currentUser.getId(), u.getId())
                .stream()
                .filter(x->x.getData().isAfter(start1)&&x.getData().isBefore(end1))
                .collect(Collectors.toList());

    }

    public void send(String msg, List<Long> listaUtilizatori,Long repliedMessage) throws Exception {
        Message message = new Message(this.currentUser.getId(), listaUtilizatori, msg, LocalDateTime.now(),repliedMessage);
        this.repoMessage.save(message);
        notifyObservers();
    }

   /* public List<Message> conversationWithAllBetweenDates(LocalDate start, LocalDate end){
        LocalDateTime start1 = LocalDateTime.of(start, LocalTime.of(10,10));
        LocalDateTime end1 = LocalDateTime.of(end, LocalTime.of(10,10));
        return StreamSupport.stream(this.repoMessage.findAll().spliterator(),false)
                .filter(x->x.getData().isAfter(start1)&&x.getData().isBefore(end1))
                .collect(Collectors.toList());
    }

    */


    public Integer noOfMessagesWith(Long id){
        return repoMessage.getConversation(currentUser.getId(), id).size();
    }





    @Override
    public void addObserver(Observer e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(x->x.update());
    }

    public Message getMessageById(Long id){
        return this.repoMessage.findOne(id);
    }

    private int page = 1;
    private int size = 12;


    public void setPageSize(int size) {
        this.size = size;
    }

    public void setPage(int page){ this.page = page;}

    private int maxConversationPage(Utilizator u){
        int aux = this.repoMessage.getConversation(currentUser.getId(), u.getId()).size();
        if(aux % size == 0)
            return aux/size;
        return aux/size + 1;

    }

    public List<Message> getConversationWithOnNextPage(Utilizator u){
        page++;
        if(page > maxConversationPage(u))
            return null;
        return getConversationWithOnPage(u, page);
    }

    public List<Message> getConversationWithOnCurrentPage(Utilizator u){
        return getConversationWithOnPage(u, page);
    }

    public List<Message> getConversationWithOnPage(Utilizator u, int page) {
        this.page=page;
        Pageable pageable = new Pageable(page, this.size);
        Page<Message> messagePage = repoMessage
                .getConversation(currentUser.getId(), u.getId(), pageable);
        return messagePage.getContent().collect(Collectors.toList());
    }





}
