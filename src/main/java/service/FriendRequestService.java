package socialnetwork.service;

import events.EventType;
import events.FriendRequestsChangedEvent;
import observer.Observable;
import observer.Observer;
import repository.paging.Page;
import repository.paging.Pageable;
import repository.paging.PagingRepository;
import socialnetwork.domain.*;
import socialnetwork.domain.FriendRequest;
import socialnetwork.repository.Repository;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FriendRequestService implements Observable {
    private socialnetwork.repository.database.FriendRequestDB friendRequestRepository;
    private Repository<socialnetwork.domain.Tuple<Long,Long>, socialnetwork.domain.Prietenie> prietenieRepository;
    private socialnetwork.repository.database.UtilizatorDB utilizatorRepository;
    private List<Observer> observers=new ArrayList<>();
    Utilizator currentUser;

    public void setCurrentUser(Utilizator currentUser) {
        this.currentUser = currentUser;
    }

    public FriendRequestService(socialnetwork.repository.database.FriendRequestDB repoRequest,
                                Repository<Tuple<Long,Long>, Prietenie> repoPrietenie,
                                socialnetwork.repository.database.UtilizatorDB repoUtilizator){
        this.friendRequestRepository = repoRequest;
        this.prietenieRepository = repoPrietenie;
        this.utilizatorRepository = repoUtilizator;

    }

    //public FriendRequest getRequest(socialnetwork.domain.Utilizator u1, socialnetwork.domain.Utilizator u2){
  //      return this.friendRequestRepository.findOne(new Tuple<Long,Long>(u1.getId(),u2.getId()));
   // }

    public void acceptRequest(Long id) throws Exception {
        FriendRequest friendRequest = friendRequestRepository.findOne(new Tuple<Long,Long>(id,currentUser.getId()));
        if(friendRequest == null){
            throw new BusinessLogicException("Cererea de prietenie nu exista.");

        }
        if(prietenieRepository.findOne(new Tuple<Long,Long>(id,currentUser.getId())) != null ||
                prietenieRepository.findOne(new Tuple<Long,Long>(currentUser.getId(),id)) != null){
            throw new BusinessLogicException("Cererea de prietenie a fost deja acceptata.");
        }
        prietenieRepository.save(new Prietenie(id,currentUser.getId()));
        friendRequestRepository.delete(friendRequest.getId());
        socialnetwork.domain.Tuple<Long, Long> oppositeID =
                new socialnetwork.domain.Tuple<Long,Long>(friendRequest.getId().getRight(), friendRequest.getId().getLeft());
        if(friendRequestRepository.findOne(oppositeID) != null)
            friendRequestRepository.delete(oppositeID);
        notifyObservers();
        this.utilizatorRepository.findOne(this.currentUser.getId()).addPrieten(this.utilizatorRepository.findOne(id));
        this.utilizatorRepository.findOne(id).addPrieten(this.utilizatorRepository.findOne(this.currentUser.getId()));
    }

    public void rejectRequest(Long id) throws SQLException, ClassNotFoundException {
        FriendRequest friendRequest = friendRequestRepository.findOne(new Tuple<Long,Long>(id,currentUser.getId()));
        if(friendRequest == null){
            throw new BusinessLogicException("Nu exista cererea de prietenie");

        }
        friendRequestRepository.delete(new Tuple<Long,Long>(id,currentUser.getId()));
        notifyObservers();
    }

    public List<FriendRequest> myFriendRequests(){
        List<FriendRequest> rez = new ArrayList<>();
        for(FriendRequest friendRequest: this.getCurrentReceivedFriendRequests())
                rez.add(friendRequest);
        return rez;
    }
    public void sendFriendRequest(Long id) throws Exception {
        FriendRequest friendRequest = new FriendRequest(this.currentUser.getId(),id);
        if(this.friendRequestRepository.findOne(new Tuple<Long,Long>(this.currentUser.getId(),id))!=null)
            throw new socialnetwork.service.BusinessLogicException("Friend Request Already Sent");
        if(this.prietenieRepository.findOne(new Tuple<Long,Long>(this.currentUser.getId(),id))!=null ||
                this.prietenieRepository.findOne(new Tuple<Long,Long>(id,this.currentUser.getId()))!=null
        )
            throw new BusinessLogicException("You are already friends");
        this.friendRequestRepository.save(friendRequest);
        notifyObservers();
    }


    public void deleteFriendRequest(Long destinationID, LocalDateTime dateTime)throws SQLException, ClassNotFoundException{
        FriendRequest friendRequest = new FriendRequest(currentUser.getId(), destinationID);
        friendRequest.setDate(dateTime);
        this.friendRequestRepository.delete(friendRequest.getId());
        notifyObservers();
    }


    @Override
    public void addObserver(Observer e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(x->x.update());
    }


    public List<socialnetwork.domain.FriendRequest> pendingFriendRequests(){
        Set<FriendRequest> aux = this.getCurrentPendingFriendRequests();

        return new ArrayList<>(aux);
    }

    private int pageForPending = 1;
    private int size = 3;

    public FriendRequest friendRequestExists(Long id1, Long id2){
        return friendRequestRepository.findOne(new socialnetwork.domain.Tuple<Long, Long>(id1, id2)) ;
    }


    public void setPageSize(int size) {
        this.size = size;
    }


    public int maxPageForPending(){
        int aux = this.friendRequestRepository.getPendingSize(currentUser.getId());
        if(aux % size == 0)
            return aux/size;
        return aux/size + 1;
    }



    public Set<FriendRequest> getCurrentPendingFriendRequests(){
        return getPendingFriendRequestsOnPage(this.pageForPending);

    }
    public Set<FriendRequest> getNextPendingFriendRequests() {
        this.pageForPending++;
        if(pageForPending > maxPageForPending())
            return null;
        return getPendingFriendRequestsOnPage(this.pageForPending);
    }

    public Set<FriendRequest> getPreviousPendingFriendRequests(){
        this.pageForPending--;
        if(pageForPending < 1)
            return null;
        return getPendingFriendRequestsOnPage(this.pageForPending);
    }

    public Set<FriendRequest> getPendingFriendRequestsOnPage(int page) {
        this.pageForPending=page;
        Pageable pageable = new Pageable(page, this.size);
        Page<FriendRequest> friendRequestPage = friendRequestRepository
                .getAllFriendRequestsFromUser(currentUser.getId(),pageable);
        return friendRequestPage.getContent().collect(Collectors.toSet());
    }


    private int pageForReceived = 1;


    public int maxPageForReceived(){
        int aux = this.friendRequestRepository.getReceivedSize(currentUser.getId());
        if(aux % size == 0)
            return aux/size;
        return aux/size + 1;
    }



    public Set<FriendRequest> getCurrentReceivedFriendRequests(){
        return getReceivedFriendRequestsOnPage(this.pageForReceived);

    }
    public Set<FriendRequest> getNextReceivedFriendRequests() {
        this.pageForReceived++;
        if(pageForReceived > maxPageForReceived())
            return null;
        return getReceivedFriendRequestsOnPage(this.pageForReceived);
    }

    public Set<FriendRequest> getPreviousReceivedFriendRequests(){
        this.pageForReceived--;
        if(pageForReceived < 1)
            return null;
        return getReceivedFriendRequestsOnPage(this.pageForReceived);
    }

    public Set<FriendRequest> getReceivedFriendRequestsOnPage(int page) {
        this.pageForReceived=page;
        Pageable pageable = new Pageable(page, this.size);
        Page<FriendRequest> friendRequestPage = friendRequestRepository
                .getAllFriendRequestsForUser(currentUser.getId(),pageable);
        return friendRequestPage.getContent().collect(Collectors.toSet());
    }
}
