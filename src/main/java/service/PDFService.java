package service;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import sun.tools.jstat.Alignment;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class PDFService {
    public void generatePDFBasedOnString(String string){
        String dest = "results/history.pdf";
        File file = new File(dest);
        file.getParentFile().mkdirs();
        PdfWriter writer = null;
        try {
            writer = new PdfWriter(dest);
            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(writer);

            // Initialize document
            Document document = new Document(pdf);

            String imageFile = "F:/Faculta/Anul 2/MAP/Seminarii/guisetup/guisetup/src/main/resources/images/coffeecherry.png";
            ImageData imageData = ImageDataFactory.create(imageFile);
            Image image = new Image(imageData);
            image.setWidth(100);
            image.setHeight(100);
            document.add(image);
            //Add paragraph to the document

            Paragraph introParagraph = new Paragraph("Your messages are here!");
            introParagraph.setTextAlignment(TextAlignment.CENTER);
            introParagraph.setFont(PdfFontFactory.createFont(FontConstants.COURIER_BOLD));
            introParagraph.setFontSize(20.0f);

            Paragraph mainParagraph = new Paragraph(string);
            mainParagraph.setFont(PdfFontFactory.createFont(FontConstants.COURIER_OBLIQUE));
            mainParagraph.setTextAlignment(TextAlignment.CENTER);

            document.add(introParagraph);
            document.add(mainParagraph);
            Desktop.getDesktop().open(new File("results/history.pdf"));
            //Close document
            document.close();
        } catch (FileNotFoundException e) {
            System.out.println("nu e bine inca.");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void generatePDFForNewFriendsAndMessages(List<socialnetwork.domain.Utilizator> friendlist, String bestFriend, String messageList){
        String dest = "results/history.pdf";
        File file = new File(dest);
        file.getParentFile().mkdirs();
        PdfWriter writer = null;
        try {
            writer = new PdfWriter(dest);
            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(writer);

            // Initialize document
            Document document = new Document(pdf);
            String imageFile = "/Faculta/Anul 2/MAP/Seminarii/guisetup/guisetup/src/main/resources/images/coffeecherry.png";
         //   String imageFile = "F:/Faculta/Anul 2/MAP/Seminarii/guisetup/guisetup/src/main/resources/images/coffeecherry.png";
            ImageData imageData = ImageDataFactory.create(imageFile);

            Image image = new Image(imageData);

            image.setWidth(100);
            image.setHeight(100);
            document.add(image);

            Paragraph newFriendsParagraph = new Paragraph("Your new friends in this time period: ");
            newFriendsParagraph.setFontSize(20.0f);
            newFriendsParagraph.setTextAlignment(TextAlignment.CENTER);
            newFriendsParagraph.setFont(PdfFontFactory.createFont(FontConstants.COURIER_BOLD));

            String aux = "";
            for (socialnetwork.domain.Utilizator utilizator : friendlist) {
                aux = aux + "\n" + utilizator;
            }

            Paragraph friendsParagraph = new Paragraph(aux);
            friendsParagraph.setFont(PdfFontFactory.createFont(FontConstants.COURIER_OBLIQUE));
            friendsParagraph.setTextAlignment(TextAlignment.CENTER);

            Paragraph bffParagraph = new Paragraph(bestFriend);
            bffParagraph.setFont(PdfFontFactory.createFont(FontConstants.COURIER_BOLD));
           // bffParagraph.setTextAlignment(TextAlignment.CENTER);
            bffParagraph.setFontSize(15.0f);


            Paragraph newMessagesParagraph = new Paragraph("Your new messages: ");
            newMessagesParagraph.setFont(PdfFontFactory.createFont(FontConstants.COURIER_BOLD));
            newMessagesParagraph.setFontSize(20.0f);
            newMessagesParagraph.setTextAlignment(TextAlignment.CENTER);

            Paragraph messagesParagraph = new Paragraph(messageList);
            messagesParagraph.setFont(PdfFontFactory.createFont(FontConstants.COURIER_OBLIQUE));
            messagesParagraph.setTextAlignment(TextAlignment.LEFT);
            document.add(newFriendsParagraph);
            document.add(friendsParagraph);
            document.add(bffParagraph);
            document.add(newMessagesParagraph);
            document.add(messagesParagraph);
            document.close();

            Desktop.getDesktop().open(new File("results/history.pdf"));


        }catch (FileNotFoundException e) {
            System.out.println("nu e bine inca.");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
