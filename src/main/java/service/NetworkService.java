package socialnetwork.service;


import domain.FriendDateDTO;
import org.mindrot.jbcrypt.BCrypt;
import events.EventType;
import events.FriendshipsChangedEvent;
import observer.Observable;
import observer.Observer;
import repository.paging.Page;
import repository.paging.Pageable;
import socialnetwork.domain.NameFriendshipDTO;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import sun.nio.ch.Util;
import utils.Crypter;


import java.sql.Array;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class NetworkService implements Observable{

    private socialnetwork.repository.database.UtilizatorDB repoUtilizatori;
    private Repository<Tuple<Long,Long>, Prietenie> repoPrietenii;
    private List<Observer> observers=new ArrayList<>();


    public NetworkService(socialnetwork.repository.database.UtilizatorDB repo, Repository<Tuple<Long,Long>, Prietenie> repo_p) {
        this.repoUtilizatori = repo;
        this.repoPrietenii = repo_p;


        for(Prietenie prietenie: repo_p.findAll()){
            Long id1 = prietenie.getId().getLeft();
            Long id2 = prietenie.getId().getRight();
            this.repoUtilizatori.findOne(id1).addPrieten(this.repoUtilizatori.findOne(id2));
            this.repoUtilizatori.findOne(id2).addPrieten(this.repoUtilizatori.findOne(id1));


        }

    }



    /**
     *
     * @param messageTask the user
     * @return adds the user in the network
     */
    public Utilizator addUtilizator(String email, String firstName, String lastName, String password) throws Exception {

        Utilizator u = new Utilizator(email, firstName, lastName, Crypter.hash(password));
        Utilizator task = repoUtilizatori.save(u);
        notifyObservers();
        return task;
    }

    public boolean verifyPassword(Utilizator u, String password){
        return Crypter.verifyHash(password, u.getHashedPassword());
    }

    /**
     *
     * @param messageTask the user
     * @return removes the user from the network
     */
    public Utilizator removeUtilizator(Utilizator messageTask) throws SQLException, ClassNotFoundException {
        for(Utilizator u: messageTask.getFriends()){
            this.repoUtilizatori.findOne(u.getId()).removePrieten(messageTask);
        }
        ArrayList<Prietenie> aux = new ArrayList<>();
        for(Prietenie p: this.repoPrietenii.findAll()){
            aux.add(p);
        }
       // Iterable<Prietenie> aux = this.repoPrietenii.findAll();
        for(Prietenie p: aux){
            if(p.getId().getLeft().equals(messageTask.getId())|| p.getId().getRight().equals(messageTask.getId()))
                this.repoPrietenii.delete(p.getId());
        }
        Utilizator task = repoUtilizatori.delete(messageTask.getId());
        notifyObservers();
        return task;
    }


    /**
     *
     * @param u1 a user
     * @param u2 another user
     * @return adds a friendship between the 2 users
     */
    public Prietenie addPrietenie(Utilizator u1, Utilizator u2){
        if(!(this.contains(u1) && this.contains(u2))) {
            throw new BusinessLogicException("Utilizatorii trebuie sa existe deja in retea.");
        }
        Prietenie prietenie = new Prietenie(u1.getId(), u2.getId(), LocalDateTime.now());
        try {
            repoPrietenii.save(prietenie);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        this.repoUtilizatori.findOne(u1.getId()).addPrieten(this.repoUtilizatori.findOne(u2.getId()));
        this.repoUtilizatori.findOne(u2.getId()).addPrieten(this.repoUtilizatori.findOne(u1.getId()));
        this.notifyObservers();

        return prietenie;
    }


    /**
     *
     * @param u1 a user
     * @param u2 another user
     * @return removes the friendship between users
     */
    public Prietenie removePrietenie(Utilizator u1, Utilizator u2) throws SQLException, ClassNotFoundException {
        if(!(this.contains(u1) && this.contains(u2))) {
            throw new BusinessLogicException("Utilizatorii trebuie sa existe deja in retea.");
        }
        Tuple<Long, Long> t = new Tuple<Long, Long>(u1.getId(), u2.getId());
        Utilizator aux1 = this.repoUtilizatori.findOne(u1.getId());
        Utilizator aux2 = this.repoUtilizatori.findOne(u2.getId());
        aux1.removePrieten(aux2);
        aux2.removePrieten(aux1);
        Prietenie p = repoPrietenii.delete(t);
        if(p == null)
            p = repoPrietenii.delete(new Tuple<Long,Long>(u2.getId(), u1.getId()));
        notifyObservers();
        return p;
    }

    public Iterable<Utilizator> getAll(){
        return repoUtilizatori.findAll();
    }

    public boolean contains(Utilizator u) {
        for(Utilizator aux: this.getAll())
            if(aux.equals(u))
                return true;

        return false;
    }

    public socialnetwork.repository.database.UtilizatorDB getRepoUtilizatori(){
        return this.repoUtilizatori;
    }

    public Repository<Tuple<Long,Long>, Prietenie>getRepoPrietenii(){
        return this.repoPrietenii;
    }




    public Utilizator getCredentials(String mail){
        Utilizator u = new Utilizator(mail, "", "");
        if(this.repoUtilizatori.findOne(u.getId())!= null)
            return this.repoUtilizatori.findOne(u.getId());
        else
            throw new socialnetwork.service.BusinessLogicException("There is no user with this e-mail");
    }

    public Utilizator getCredentialsById(Long id){
        return this.repoUtilizatori.findOne(id);
    }

    public List<Utilizator> friendshipsFor(Utilizator u){
        List<Prietenie> prietenii = StreamSupport.stream(repoPrietenii.findAll().spliterator(),false)
                .collect(Collectors.toList());
        return prietenii
                .stream()
                .filter(x->x.getId().getRight().equals(u.getId()) || x.getId().getLeft().equals(u.getId()))
                .map(x->{
                    Utilizator aux = null;
                    if(x.getId().getLeft().equals(u.getId())) {
                        aux = this.repoUtilizatori.findOne(x.getId().getRight());
                    }
                    else {
                        aux = this.repoUtilizatori.findOne(x.getId().getLeft());
                    }
                    return aux;
                })
                .collect(Collectors.toList());
    }

    public List<NameFriendshipDTO> friendshipsByUser(Utilizator u){
        ArrayList<Prietenie> prietenii = new ArrayList<>();
        for(Prietenie p: this.repoPrietenii.findAll())
            prietenii.add(p);
        return prietenii
                .stream()
                .filter(x->x.getId().getRight().equals(u.getId()) || x.getId().getLeft().equals(u.getId()))
                .map(x->{
                    Utilizator aux = null;
                    if(x.getId().getLeft().equals(u.getId())) {
                        aux = this.repoUtilizatori.findOne(x.getId().getRight());
                    }
                    else {
                        aux = this.repoUtilizatori.findOne(x.getId().getLeft());
                    }
                    return new NameFriendshipDTO(aux.getFirstName(),aux.getLastName(),x.getDate());
                })
                .collect(Collectors.toList());
    }

    public List<NameFriendshipDTO> friendshipOnDate(Utilizator u, int month, int year){
        List<NameFriendshipDTO> rez = friendshipsByUser(u);

        return rez.stream()
                .filter(x-> {return x.getDate().getMonth().getValue()==month
                                    && x.getDate().getYear() == year;})
                .collect(Collectors.toList());

    }


    public List <Utilizator> friendshipsByUserDate(Utilizator u, LocalDate start, LocalDate end){
        return StreamSupport.stream(this.repoPrietenii.findAll().spliterator(),false)
                .filter(p-> p.getId().getLeft().equals(u.getId())|| p.getId().getRight().equals(u.getId()))
                .filter(p-> p.getDate().isAfter(LocalDateTime.of(start, LocalTime.of(10,10))) &&
                            p.getDate().isBefore(LocalDateTime.of(end, LocalTime.of(10, 10))))
                .map(p->{
                    Utilizator prieten = null;
                    if(p.getId().getLeft().equals(u.getId()))
                        prieten = getCredentialsById(p.getId().getRight());
                    else
                        prieten = getCredentialsById(p.getId().getLeft());
                    return prieten;
                })
                .collect(Collectors.toList());

    }

    @Override
    public void addObserver(Observer e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(x->x.update());
    }

    int userPage = 1;

    public void setUserSize(int userSize) {
        this.userSize = userSize;
    }

    public void setUserPage(int userPage) {
        this.userPage = userPage;
    }

    int userSize = 9;

    public List<Utilizator> getFriendsOnPage(Utilizator u, int page){
        userPage = page;
        return repoUtilizatori.getFriendsOnPage(u.getId(), new Pageable(userPage, userSize))
                .getContent()
                .map(x-> repoUtilizatori.findOne(x))
                .collect(Collectors.toList());
    }

    public int getUserPage(){
        return userPage;
    }

    public int maxUserPageForUser(Utilizator u){

        int aux = friendshipsFor(u).size();
        if(aux % userSize == 0)
            return aux/userSize;
        return aux/userSize + 1;
    }



    public void goNextUserPage(){
        userPage++;
    }

    public void goPreviousUserPage(){
        userPage--;
    }

    public List<Utilizator> getFriendsOnCurrentPage(Utilizator u){
        return getFriendsOnPage(u, userPage);
    }


    int searchUserPage = 1;
    int searchSize = 9;

    public int getSearchUserPage() {
        return searchUserPage;
    }

    public void setSearchUserPage(int searchUserPage) {
        this.searchUserPage = searchUserPage;
    }

    public int getSearchSize() {
        return searchSize;
    }

    public void setSearchSize(int searchSize) {
        this.searchSize = searchSize;
    }

    public void goNextSearchPage(){
        searchUserPage++;
    }
    public void goPreviousSearchPage(){
        searchUserPage--;
    }

    public int getMaxSearchPage(){
        int aux = (int) StreamSupport
                .stream(repoUtilizatori
                        .findAll()
                        .spliterator(), false)
                .count();
        if(aux % userSize == 0)
            return aux/userSize;
        return aux/userSize + 1;
    }
    public List<Utilizator> getCurrentSearchPageContaining(String string){
        return getSearchOnPageContaining(string, searchUserPage);
    }

    public List<Utilizator> getSearchOnPageContaining(String string, int page){
        searchUserPage = page;
        return this.repoUtilizatori.getUsersOnPageContainingString(string, new Pageable(searchUserPage, searchSize))
                .getContent()
                .collect(Collectors.toList());
    }
}
