package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import socialnetwork.domain.Utilizator;
import sun.nio.ch.Net;

import java.util.Optional;

public class RegisterScreenController {
    socialnetwork.service.NetworkService networkService;

    @FXML
    Label logoLabel;

    @FXML
    TextField firstNameTextField;

    @FXML
    TextField lastNameTextField;

    @FXML
    TextField emailTextField;

    @FXML
    PasswordField passwordTextField;

    @FXML
    PasswordField confirmpasswordTextField;

    @FXML
    Button registerButton;

    @FXML
    Label registerErrorLabel;


    @FXML
    public void initialize(){

        logoLabel.setStyle("-fx-font-size: 40");

        registerErrorLabel.setId("alert-label");
        registerButton.setId("register-button");
    }

    @FXML
    public void handleRegister() {
        if (firstNameTextField.getText().isEmpty() ||
                lastNameTextField.getText().isEmpty() ||
                emailTextField.getText().isEmpty() ||
                passwordTextField.getText().isEmpty() ||
                confirmpasswordTextField.getText().isEmpty()) {

            registerErrorLabel.setText("One or more of the fields are empty! Please fill\n" +
                    " all the fields");
            return;
        }

        if (!passwordTextField.getText().equals(confirmpasswordTextField.getText())) {

            registerErrorLabel.setText("The passwords don't match");
            return;
        }
        try {
            Utilizator u = networkService.getCredentials(emailTextField.getText());

            registerErrorLabel.setText("An account with this e-mail already exists. Please" +
                    "\n Login or choose a new e-mail");
            return;
        } catch (socialnetwork.service.BusinessLogicException b) {
            try {

                networkService.addUtilizator(emailTextField.getText(), firstNameTextField.getText(), lastNameTextField.getText(), passwordTextField.getText());


                FXMLLoader messageLoader = new FXMLLoader();
                messageLoader.setLocation(getClass().getResource("/loginScreen.fxml"));
                VBox messageTaskLayout = messageLoader.load();
                LoginScreenController loginScreenController = messageLoader.getController();
                loginScreenController.setService(this.networkService);
                loginScreenController.loginErrorLabel.setTextFill(Color.GREEN);
                loginScreenController.loginErrorLabel.setText("You are now registered and can login");
                Stage stage = new Stage();
                stage.setTitle("Caffeinstagram");
                stage.setScene(new Scene(messageTaskLayout, 299, 470));
                //stage.getIcons().add(new Image("/images/coffeelogo.png"));
                stage.show();
                Stage stage1 = (Stage) logoLabel.getScene().getWindow();
                stage1.close();
            }

         catch (Exception e) {

            registerErrorLabel.setText("There already is a user account with " +
                    "\nthis email.");
        }
    }


    }

    public void setNetworkService(socialnetwork.service.NetworkService networkService){
        this.networkService = networkService;
    }

}
