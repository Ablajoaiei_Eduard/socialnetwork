package controller;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Text;
import domain.Eveniment;
import domain.FriendNoMessages;
import domain.UserFriendRequestDateDTO;
import domain.validators.EvenimentValidator;
import events.FriendRequestsChangedEvent;
import events.FriendshipsChangedEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.util.Callback;
import jdk.vm.ci.meta.Local;
import observer.Observer;
import repository.database.EvenimentDB;
import repository.paging.PagingRepository;
import service.EvenimentService;
import service.PDFService;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.repository.database.UtilizatorDB;
import socialnetwork.service.FriendRequestService;
import socialnetwork.service.MessageService;
import socialnetwork.service.NetworkService;

import javax.naming.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MainScreenController implements Observer{
    public static final int FRIENDREQUESTSNOONPAGE = 3;
    socialnetwork.service.FriendRequestService friendRequestService;
    socialnetwork.service.MessageService messageService;
    socialnetwork.service.NetworkService networkService;
    EvenimentService evenimentService;
    socialnetwork.domain.Utilizator currentConversationPartner;
    socialnetwork.domain.Message rmsg;


    String currentSearchedUser;
    NetworkService auxiliaryService;

    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
        this.networkService.addObserver(this);
    }

    socialnetwork.domain.Utilizator user;

    ObservableList<socialnetwork.domain.Utilizator> friendListModel = FXCollections.observableArrayList();
    ObservableList<UserFriendRequestDateDTO> pendingFriendRequests = FXCollections.observableArrayList();
    ObservableList<UserFriendRequestDateDTO> receivedFriendRequests = FXCollections.observableArrayList();

    ScrollBar scrollBar;

    @FXML
    Button nextFriendListPageButton;

    @FXML
    Label historyErrorLabel;

    @FXML
    Button previousFriendListPageButton;

    @FXML
    Button nextReceivedFriendRequestPageButton;
    @FXML
    Button previousReceivedFriendRequestPageButton;

    @FXML
    TableView<Eveniment> yourEventsTableView;

    @FXML
    Button previousPendingFriendRequestPageButton;

    @FXML
    Button nextPendingFriendRequestPageButton;

    @FXML
    Label logoLabel;

    @FXML
    VBox friendRequestArea;

    @FXML
    Label loggedLabel;

    @FXML
    Button getMoreFriendsButton;

    @FXML
    Label sendFriendRequestErrorLabel;
    @FXML
    ListView<UserFriendRequestDateDTO> receivedFriendRequestListView;

    @FXML
    ListView<UserFriendRequestDateDTO> pendingFriendRequestListView;


    @FXML
    ListView<socialnetwork.domain.Utilizator> friendListListView;

    @FXML
    TextField sendFriendRequestTextField;

    @FXML
    Button sendFriendRequestButton;

    @FXML
    TextArea messagingArea;

    @FXML
    ListView<socialnetwork.domain.Utilizator> searchResultListView;

    @FXML
    VBox searchResultArea;

    @FXML
    Button previousSearchPageButton;

    @FXML
    Button nextSearchPageButton;

    @FXML
    TextField searchBar;

    @FXML
    Button searchButton;

    @FXML
    Button sendMessageButton;

    @FXML
    Circle circle;

    @FXML
    Label circleLabel;

    @FXML
    TextField sendMessageTextField;

    @FXML
    Button messageMoreButton;

    @FXML
    TableColumn nameColumn;

    @FXML
    TableColumn dateColumn;

    @FXML
    Label replyLabel;

    @FXML
    VBox messageCenterArea;

    @FXML
    VBox sendMessageToManyCenterArea;

    @FXML
    ListView<socialnetwork.domain.Utilizator> sendMessageToManyFriendListListView;

    @FXML
    ListView<socialnetwork.domain.Utilizator> sendMessageToManyChosenFriendsListView;

    @FXML
    Button sendMessageToManyAddButton;

    @FXML
    Button sendMessageToManyRemoveButton;

    @FXML
    TextField sendMessageToManyTextField;

    @FXML
    Button yourEventsButton;

    @FXML
    Button sendMessageToManySendButton;

    @FXML
    Button sendMessageToManyCloseButton;

    @FXML
    Button moreForUserButton;

    @FXML
    VBox historyArea;

    @FXML
    DatePicker startDatePicker;

    @FXML
    DatePicker endDatePicker;

    @FXML
    ComboBox<String> historyTypeComboBox;

    @FXML
    ComboBox<socialnetwork.domain.Utilizator> historyWithFriendsComboBox;

    @FXML
    Button exportButton;

    @FXML
    TextArea historyTextArea;


    @FXML
    BorderPane userProfileArea;

    @FXML
    Label userProfileNameLabel;

    @FXML
    Label userProfileEmailLabel;

    @FXML
    ListView<socialnetwork.domain.Utilizator> userProfileFriendsListView;

    @FXML
    Button userProfilePreviousFriendsPageButton;

    @FXML
    Button userProfileNextFriendsPageButton;


    @FXML
    Label addEventErrorLabel;

    @FXML
    Button userProfileSendRemoveDeleteButton;

    @FXML
    Label userProfileEventsLabel;

    @FXML
    ListView<Eveniment> userProfileEventListView;

    @FXML
    Button userProfilePreviousEventPage;

    @FXML
    Button userProfileNextEventPage;

    @FXML
    VBox addEventArea;

    @FXML
    TextField eventNameTextField;

    @FXML
    DatePicker eventDatePicker;

    @FXML
    Button addEventButton;

    @FXML
    Button closeEventButton;






    private ScrollBar getTextAreaScrollBar(TextArea textArea) {
        ScrollBar scrollbar = null;

        for (Node node : textArea.lookupAll(".scroll-bar")) {
            if (node instanceof ScrollBar) {
                ScrollBar bar = (ScrollBar) node;
                if (bar.getOrientation().equals(Orientation.VERTICAL)) {
                    scrollbar = bar;
                }
            }
        }
        return scrollbar;
    }


    public void setMessagingAreaScrollBar(){
        scrollBar = getTextAreaScrollBar(messagingArea);
    }

    private void initEventArea(){

        addEventButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(eventDatePicker.getValue() == null){
                    addEventErrorLabel.setTextFill(Color.RED);
                    addEventErrorLabel.setText("Pick a date");
                    return;
                }
                if(eventDatePicker.getValue().isBefore(ChronoLocalDate.from(LocalDateTime.now()))){

                    addEventErrorLabel.setTextFill(Color.RED);
                    addEventErrorLabel.setText("Pick a date in the future");

                    return;
                }
                else if(eventNameTextField.getText().isEmpty()){
                    addEventErrorLabel.setTextFill(Color.RED);
                    addEventErrorLabel.setText("Enter a name for your event!");
                    return;
                }
                evenimentService.addEveniment(new Eveniment(user.getId(), eventNameTextField.getText(), eventDatePicker.getValue()));
                addEventErrorLabel.setTextFill(Color.GREEN);
                addEventErrorLabel.setText("Event added");

            }
        });

        closeEventButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setCenterToMessageArea();
            }
        });


    }


    private void initSearch() {

        searchResultListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(searchResultListView.getSelectionModel().isEmpty())
                    return;
                setCenterToUserProfileArea();
                socialnetwork.domain.Utilizator selected = searchResultListView.getSelectionModel().getSelectedItem();
                if(user.equals(selected)){
                    setCenterToMessageArea();
                    return;
                }

                userProfileNameLabel.setText(selected.getLastName()+ " " +
                        selected.getFirstName());
                userProfileEmailLabel.setText(selected.getMail());

                if(networkService.friendshipsFor(user).contains(selected)){
                    userProfileSendRemoveDeleteButton.setText("Remove Friend");

                }
                else if(friendRequestService.friendRequestExists(user.getId(), selected.getId()) != null){
                    userProfileSendRemoveDeleteButton.setText("Delete Friend Request");

                }
                else{
                    userProfileSendRemoveDeleteButton.setText("Send Friend Request");

                }

                userProfileSendRemoveDeleteButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if(userProfileSendRemoveDeleteButton.getText().equals("Remove Friend")){
                            try {
                                networkService.removePrietenie(user, selected);
                                userProfileSendRemoveDeleteButton.setText("Send Friend Request");
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            } catch (ClassNotFoundException e) {
                                Alert msg = new Alert(Alert.AlertType.ERROR, e.getMessage());
                                msg.show();
                            }

                        }
                        else if(userProfileSendRemoveDeleteButton.getText().equals("Delete Friend Request")){
                            try {
                                socialnetwork.domain.FriendRequest fr = friendRequestService.friendRequestExists(
                                        user.getId(),
                                        selected.getId()
                                );
                                friendRequestService.deleteFriendRequest(fr.getDestination(), fr.getDate());
                                userProfileSendRemoveDeleteButton.setText("Send Friend Request");
                            } catch (SQLException | ClassNotFoundException e ) {
                                Alert msg = new Alert(Alert.AlertType.ERROR, e.getMessage());
                                msg.show();
                            }
                        }
                        else{
                            try {
                                friendRequestService.sendFriendRequest(selected.getId());
                                userProfileSendRemoveDeleteButton.setText("Delete Friend Request");
                            } catch (Exception e) {
                                Alert msg = new Alert(Alert.AlertType.ERROR, e.getMessage());
                                msg.show();
                            }
                        }
                    }
                });

                userProfileEventsLabel.setText("Events of " + userProfileNameLabel.getText());

               // NetworkService networkServiceForSearch = new NetworkService(networkService.getRepoUtilizatori(), networkService.getRepoPrietenii());
               // networkServiceForSearch.setUser
                auxiliaryService.setUserPage(1);
                List<socialnetwork.domain.Utilizator> f = auxiliaryService.getFriendsOnCurrentPage(selected);
                userProfileFriendsListView.getItems().clear();
                userProfileEventListView.getItems().clear();
                userProfileFriendsListView.getItems().addAll(f);
                userProfilePreviousFriendsPageButton.setDisable(true);
                if(auxiliaryService.maxUserPageForUser(selected) == 1)
                    userProfileNextFriendsPageButton.setDisable(true);

                if(evenimentService.getMaxPage(selected.getId()) <= 1)
                    userProfileNextEventPage.setDisable(true);




                userProfileEventListView.getItems().addAll(evenimentService.getCurrentEvenimentPageForUser(selected.getId()));




            }
        });

        userProfilePreviousFriendsPageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                auxiliaryService.goPreviousUserPage();
                userProfileNextFriendsPageButton.setDisable(false);
                socialnetwork.domain.Utilizator selected = networkService.getCredentials(userProfileEmailLabel.getText());
                if(auxiliaryService.getUserPage() <= 1)
                    userProfilePreviousFriendsPageButton.setDisable(true);
                List<socialnetwork.domain.Utilizator> f = auxiliaryService.getFriendsOnCurrentPage(selected);
                userProfileFriendsListView.getItems().clear();
                userProfileFriendsListView.getItems().addAll(f);
            }
        });

        userProfileNextFriendsPageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                auxiliaryService.goNextUserPage();
                userProfilePreviousFriendsPageButton.setDisable(false);
                socialnetwork.domain.Utilizator selected = networkService.getCredentials(userProfileEmailLabel.getText());
                if(auxiliaryService.getUserPage() >= auxiliaryService.maxUserPageForUser(selected))
                    userProfileNextFriendsPageButton.setDisable(true);
                List<socialnetwork.domain.Utilizator> f = auxiliaryService.getFriendsOnCurrentPage(selected);
                userProfileFriendsListView.getItems().clear();
                userProfileFriendsListView.getItems().addAll(f);
            }
        });

        searchButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setCenterToSearchResultArea();
                currentSearchedUser = searchBar.getText();
                searchResultArea.setDisable(false);
                searchResultListView.getItems().clear();
                searchResultListView.getItems().addAll(networkService.getCurrentSearchPageContaining(
                        searchBar.getText()
                ));


            }
        });

        previousSearchPageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                searchResultListView.getItems().clear();
                networkService.goPreviousSearchPage();
                nextSearchPageButton.setDisable(false);
                if(networkService.getSearchUserPage() <= 1)
                    previousSearchPageButton.setDisable(true);
                searchResultListView.getItems().addAll(networkService.getCurrentSearchPageContaining(
                        currentSearchedUser));
            }
        });

        nextSearchPageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                searchResultListView.getItems().clear();
                networkService.goNextSearchPage();
                previousSearchPageButton.setDisable(false);
                if(networkService.getSearchUserPage() >= networkService.getMaxSearchPage())
                    nextSearchPageButton.setDisable(true);
                searchResultListView.getItems().addAll(networkService.getCurrentSearchPageContaining(
                        currentSearchedUser));
            }
        });

        userProfilePreviousEventPage.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                userProfileEventListView.getItems().clear();
                evenimentService.goPreviousPage();
                userProfileNextEventPage.setDisable(false);
                if(evenimentService.getPage() <= 1)
                    userProfilePreviousEventPage.setDisable(true);
                userProfileEventListView.getItems().addAll(evenimentService
                        .getCurrentEvenimentPageForUser(
                                networkService.getCredentials(userProfileEmailLabel.getText()).getId()
                        ));


            }
        });

        userProfileNextEventPage.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                socialnetwork.domain.Utilizator selected = networkService
                        .getCredentials(userProfileEmailLabel.getText());
                userProfileEventListView.getItems().clear();
                evenimentService.goNextPage();
                userProfilePreviousEventPage.setDisable(false);
                if(evenimentService.getPage() >= evenimentService.getMaxPage(selected.getId()))
                    userProfileNextEventPage.setDisable(true);
                userProfileEventListView.getItems().addAll(evenimentService
                        .getCurrentEvenimentPageForUser(
                                selected.getId()
                        ));


            }
        });
        ContextMenu contextMenu = new ContextMenu();
        userProfileEventListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(userProfileEventListView.getSelectionModel().isEmpty())
                    return;
                contextMenu.getItems().clear();
                Eveniment selectedEvent = userProfileEventListView.getSelectionModel().getSelectedItem();
                if(!selectedEvent.getSubscribers().contains(user.getId())){
                    MenuItem going = new MenuItem("I'm going!");

                    going.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            evenimentService.registerUserToEvent(selectedEvent, user.getId());
                            selectedEvent.getSubscribers().add(user.getId());

                        }
                    });

                    contextMenu.getItems().add(going);
                    contextMenu.show(userProfileEventListView,
                            Side.LEFT,
                            -75,
                            23 * userProfileEventListView.getSelectionModel().getSelectedIndex());

                }
                else if(evenimentService.userGetsNotifiedByEvent(selectedEvent, user.getId())){
                    MenuItem stopNotifying = new MenuItem("Don't notify me for this event");

                    stopNotifying.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            evenimentService.unsubscribeUserToEvent(user.getId(), selectedEvent);
                        }
                    });

                    contextMenu.getItems().add(stopNotifying);
                    contextMenu.show(userProfileEventListView,
                            Side.LEFT,
                            -140,
                            23* userProfileEventListView.getSelectionModel().getSelectedIndex()
                            );
                }
                else if(!evenimentService.userGetsNotifiedByEvent(selectedEvent, user.getId())){
                    MenuItem startNotifying = new MenuItem("Notify me for this event");

                    startNotifying.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            evenimentService.subscribeUserToEvent(user.getId(), selectedEvent);
                        }
                    });

                    contextMenu.getItems().add(startNotifying);
                    contextMenu.show(userProfileEventListView,
                            Side.LEFT,
                            -140,
                            23* userProfileEventListView.getSelectionModel().getSelectedIndex()
                    );
                }
            }
        });

    }
    private void initLeftBorder(){

        //setting buttons to disabled
        previousFriendListPageButton.setDisable(true);



        //context menu for the friend list
        ContextMenu friendListContextMenu = new ContextMenu();
        MenuItem removeButton = new MenuItem("Remove");
        MenuItem seeConversationButton = new MenuItem("See conversation");
        friendListContextMenu.getItems().addAll(removeButton, seeConversationButton);

        //context menu for "more for user"
        ContextMenu moreForUserContextMenu = new ContextMenu();
        MenuItem addEvenimentButton = new MenuItem("Add an event");
        MenuItem historyButton = new MenuItem("History");
        MenuItem logoutButton = new MenuItem("Log out");
        moreForUserContextMenu.getItems().addAll(addEvenimentButton, historyButton, logoutButton);



        //implementing handlers

        addEvenimentButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                addEventErrorLabel.setText("");
                setCenterToEventArea();

            }
        });

        getMoreFriendsButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sendFriendRequestErrorLabel.setText("");
                setCenterToFriendRequestArea();
            }
        });
        removeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {


                try {
                    networkService.removePrietenie(user, friendListListView.getSelectionModel().getSelectedItem());
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                } catch (ClassNotFoundException e) {
                    Alert msg = new Alert(Alert.AlertType.ERROR, e.getMessage());
                    msg.show();
                }
            }
        });

        seeConversationButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                setCenterToMessageArea();
                messageService.setPage(1);
                scrollBar = (ScrollBar)messagingArea.lookup(".scroll-bar:vertical");
                scrollBar.valueProperty().addListener((observable, oldValue, newValue) -> {
                    double position = newValue.doubleValue();
                    ScrollBar scroll = getTextAreaScrollBar(messagingArea);
                    if (position == scroll.getMax()) {
                        messageService.getConversationWithOnNextPage(currentConversationPartner);
                        List<socialnetwork.domain.Message> list =
                                messageService
                                        .getConversationWithOnCurrentPage(currentConversationPartner);
                        if(list == null)
                            return;
                        for(socialnetwork.domain.Message x: list){
                            String aux = x.getData().toLocalDate()
                                    + " "
                                    + x.getData().toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm"))
                                    + " ";
                            if(x.getFrom().equals(user.getId()))
                                aux += "Me: ";
                            else
                                aux += (networkService.getCredentialsById(x.getFrom()) + ": ");
                            if(x.getRepliedMessage() != null)
                                aux += "Replying to: " + messageService.getMessageById(x.getRepliedMessage()).getMessage() + "\n";
                            aux = aux
                                    + " "
                                    + x.getMessage();
                            messagingArea.setText(messagingArea.getText() + "\n" + aux);}

                    }


                });

                replyLabel.setVisible(false);
                currentConversationPartner = friendListListView.getSelectionModel().getSelectedItem();
                sendMessageButton.setDisable(false);
                messageMoreButton.setDisable(false);
                List<socialnetwork.domain.Message> list =
                        messageService
                                .getConversationWithOnCurrentPage(currentConversationPartner);

                messagingArea.clear();

                for(socialnetwork.domain.Message x: list){
                    String aux = x.getData().toLocalDate()
                            + " "
                            + x.getData().toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm"))
                            + " ";
                    if(x.getFrom().equals(user.getId()))
                        aux += "Me: ";
                    else
                        aux += (networkService.getCredentialsById(x.getFrom()) + ": ");
                    if(x.getRepliedMessage() != null)
                        aux += "Replying to: " + messageService.getMessageById(x.getRepliedMessage()).getMessage() + "\n";
                    aux = aux
                            + " "
                            + x.getMessage();
                    messagingArea.setText(messagingArea.getText() + "\n" + aux);}
             //   messagingArea.positionCaret(messagingArea.getText().length()/50);

            }
        });


/*       messagingArea.setOnScroll(new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                if(event.toString().contains("boundsType=LOGICAL_VERTICAL_CENTER")){
                    System.out.println("Sanatate!!\n");
                    messageService.getConversationWithOnNextPage(currentConversationPartner);
                    seeConversationButton.fire();
                    if(event.getTotalDeltaX())
                }


            }
        });
*/
        friendListListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(friendListListView.getSelectionModel().isEmpty())
                    return;
                friendListContextMenu.show(friendListListView, Side.RIGHT, 0,23 * friendListListView.getSelectionModel().getSelectedIndex());
            }
        });

        moreForUserButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                moreForUserContextMenu.show(moreForUserButton, Side.BOTTOM, 0, 0);
            }
        });

        historyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                setCenterToHistoryArea();


            }
        });
        logoutButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader messageLoader = new FXMLLoader();
                messageLoader.setLocation(getClass().getResource("/loginScreen.fxml"));

                VBox root = null;
                try {
                    root = messageLoader.load();
                    LoginScreenController loginScreenController = messageLoader.getController();
                    loginScreenController.setService(networkService);
                    Stage stage = new Stage();
                    stage.setScene(new Scene(root, 299, 503));
                    stage.show();
                    Stage stage1 = (Stage) loggedLabel.getScene().getWindow();
                    stage1.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        nextFriendListPageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                networkService.goNextUserPage();
                previousFriendListPageButton.setDisable(false);
                if(networkService.getUserPage() >= networkService.maxUserPageForUser(user))
                    nextFriendListPageButton.setDisable(true);
                update();
            }
        });

        previousFriendListPageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                networkService.goPreviousUserPage();
                nextFriendListPageButton.setDisable(false);
                if(networkService.getUserPage() == 1)
                    previousFriendListPageButton.setDisable(true);
                update();
            }
        });

    }

    public void initFriendRequestArea(){
        nextPendingFriendRequestPageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                friendRequestService.getNextPendingFriendRequests();
                previousPendingFriendRequestPageButton.setDisable(false);
                if(friendRequestService.getNextPendingFriendRequests() == null)
                    nextPendingFriendRequestPageButton.setDisable(true);
                friendRequestService.getPreviousPendingFriendRequests();
                update();

            }
        });

        previousPendingFriendRequestPageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                friendRequestService.getPreviousPendingFriendRequests();
                if(friendRequestService.getPreviousPendingFriendRequests() == null)
                    previousPendingFriendRequestPageButton.setDisable(true);
                friendRequestService.getNextPendingFriendRequests();
                nextPendingFriendRequestPageButton.setDisable(false);
                update();
            }
        });

        nextReceivedFriendRequestPageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                friendRequestService.getNextReceivedFriendRequests();
                previousReceivedFriendRequestPageButton.setDisable(false);
                if(friendRequestService.getNextReceivedFriendRequests() == null)
                    nextReceivedFriendRequestPageButton.setDisable(true);
                friendRequestService.getPreviousReceivedFriendRequests();
                update();
            }
        });

        previousReceivedFriendRequestPageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                friendRequestService.getPreviousReceivedFriendRequests();
                if(friendRequestService.getPreviousReceivedFriendRequests() == null)
                    previousReceivedFriendRequestPageButton.setDisable(true);
                friendRequestService.getNextReceivedFriendRequests();
                nextReceivedFriendRequestPageButton.setDisable(false);
                update();
            }
        });
    }

    public void setFriendRequestService(FriendRequestService friendRequestService) {
        this.friendRequestService = friendRequestService;
        friendRequestService.addObserver(this);

    }

    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;

    }

    public void setEvenimentService(EvenimentService evenimentService){
        this.evenimentService = evenimentService;
    }


    public List<Eveniment> notifyUserForEvents(socialnetwork.domain.Utilizator user){

        List<Eveniment> rez = new ArrayList<>();

        for(Eveniment eveniment: evenimentService.getSubscribedEventsForUser(user)){
            LocalDate today = LocalDate.now();
           // yourEventsTableView.getItems().add(eveniment);
            rez.add(eveniment);
            if(eveniment.getDate().equals(today.plusDays(1)) ||
                    eveniment.getDate().equals(today.plusDays(2)) ||
                    eveniment.getDate().equals(today)
            )
                System.out.println("stuff");
        }
        return rez;
       // Alert alert = new Alert(Alert.AlertType.INFORMATION, allEvents);
       // alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
       // alert.showAndWait();
    }

    public void setUser(socialnetwork.domain.Utilizator u){
        this.user = u;
        sendMessageButton.setDisable(true);



        try{
            socialnetwork.repository.database.MessageDB messageRepository =
                    new socialnetwork.repository.database.MessageDB(new socialnetwork.domain.validators.MessageValidator());

           socialnetwork.repository.database.FriendRequestDB
                requestRepository = new socialnetwork.repository.database.FriendRequestDB(new socialnetwork.domain.validators.FriendRequestValidator());
            UtilizatorDB utilizatorRepository =
                    new UtilizatorDB(new UtilizatorValidator());
            socialnetwork.repository.Repository<socialnetwork.domain.Tuple<Long,Long>, socialnetwork.domain.Prietenie> prietenieRepository=
                    new socialnetwork.repository.database.PrietenieDB(new socialnetwork.domain.validators.PrietenieValidator());
            EvenimentDB evenimentDB = new EvenimentDB(new EvenimentValidator());
            this.setNetworkService(new NetworkService(utilizatorRepository, prietenieRepository));
            this.setMessageService(new MessageService(messageRepository));
            this.setFriendRequestService(new FriendRequestService(requestRepository, prietenieRepository, utilizatorRepository));
            this.setEvenimentService(new EvenimentService(evenimentDB));
            this.auxiliaryService = new NetworkService(utilizatorRepository, prietenieRepository);
            auxiliaryService.setUserSize(6);
            this.messageService.setCurrentUser(u);
            this.friendRequestService.setCurrentUser(u);
            searchResultArea.setDisable(true);
            searchResultArea.setVisible(false);
            messagingArea.setEditable(false);


            update();
            //setting up the friend request page buttons
            previousPendingFriendRequestPageButton.setDisable(true);
            previousReceivedFriendRequestPageButton.setDisable(true);

            if(friendRequestService.getNextPendingFriendRequests() == null)
                nextPendingFriendRequestPageButton.setDisable(true);
            friendRequestService.getPreviousPendingFriendRequests();

            if(friendRequestService.getNextReceivedFriendRequests() == null)
                nextReceivedFriendRequestPageButton.setDisable(true);
            friendRequestService.getPreviousReceivedFriendRequests();

            if(networkService.getUserPage() >= networkService.maxUserPageForUser(user))
                nextFriendListPageButton.setDisable(true);





            initYourEventsArea();

        }
        catch(Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            alert.show();
        }

    }


    private void initYourEventsArea(){

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        yourEventsTableView.setItems(FXCollections.observableArrayList(
                evenimentService.getSubscribedEventsForUser(user).stream()
                        .sorted(Comparator.comparing(Eveniment::getDate))
                        .collect(Collectors.toList())
        ));
        PseudoClass higlighted = PseudoClass.getPseudoClass("highlighted");
        yourEventsTableView.setRowFactory(tableView -> {
            TableRow<Eveniment> row = new TableRow<>();

            row.itemProperty().addListener((obs, oldEvent, newEvent) ->{
                    if(newEvent == null)
                        return;
                    LocalDate today = LocalDate.now();
                    row.pseudoClassStateChanged(higlighted,
                            newEvent.getDate().equals(today.plusDays(1)) ||
                                    newEvent.getDate().equals(today.plusDays(2)) ||
                                    newEvent.getDate().equals(today)
                            );});
            return row;
        });

        yourEventsButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setCenterToYourEventsArea();
                circle.setVisible(false);
                circleLabel.setVisible(false);
            }
        });

        if(evenimentService.getSubscribedEventsForUser(user).stream()
                .noneMatch(event -> event.getDate().equals(LocalDate.now().plusDays(1)) ||
                event.getDate().equals(LocalDate.now().plusDays(2)) ||
                event.getDate().equals(LocalDate.now()))
        ){
            circle.setVisible(false);
            circleLabel.setVisible(true);
        }
        else{
            circleLabel.setText(String.valueOf(evenimentService.getSubscribedEventsForUser(user).stream()
                    .filter(event -> event.getDate().equals(LocalDate.now().plusDays(1)) ||
                            event.getDate().equals(LocalDate.now().plusDays(2)) ||
                            event.getDate().equals(LocalDate.now()))
                    .count())
            );
            if(Long.parseLong(circleLabel.getText()) > 9)
                circleLabel.setText("9+");
        }

        ContextMenu contextMenu = new ContextMenu();

        yourEventsTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                /*
                contextMenu.show(userProfileEventListView,
                            Side.LEFT,
                            -140,
                            23* userProfileEventListView.getSelectionModel().getSelectedIndex()
                    );
                 */
                if(yourEventsTableView.getSelectionModel().getSelectedItem() == null)
                    return;
                contextMenu.getItems().clear();
                MenuItem stopNotifyingMenuItem = new MenuItem("Don't notify me about this event.");
                stopNotifyingMenuItem.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        evenimentService.unsubscribeUserToEvent(user.getId(),
                                yourEventsTableView.getSelectionModel().getSelectedItem());
                        yourEventsTableView.setItems(FXCollections.observableArrayList(
                                evenimentService.getSubscribedEventsForUser(user).stream()
                                        .sorted(Comparator.comparing(Eveniment::getDate))
                                        .collect(Collectors.toList())));
                    }
                });
                contextMenu.getItems().add(stopNotifyingMenuItem);
                contextMenu.show(yourEventsTableView,
                        Side.LEFT,
                        -200,
                        22 + 29* yourEventsTableView.getSelectionModel().getSelectedIndex()
                );

            }
        });
    }

    private void setCenterToEventArea(){
        friendRequestArea.setVisible(false);
        messageCenterArea.setVisible(false);
        sendMessageToManyCenterArea.setVisible(false);
        searchResultArea.setVisible(false);
        userProfileArea.setVisible(false);
        historyArea.setVisible(false);
        yourEventsTableView.setVisible(false);
        addEventArea.setVisible(true);
    }

    private void setCenterToFriendRequestArea(){
        friendRequestArea.setVisible(true);
        messageCenterArea.setVisible(false);
        sendMessageToManyCenterArea.setVisible(false);
        searchResultArea.setVisible(false);
        userProfileArea.setVisible(false);
        historyArea.setVisible(false);
        yourEventsTableView.setVisible(false);
        addEventArea.setVisible(false);
    }

    private void setCenterToMessageArea(){
        friendRequestArea.setVisible(false);
        messageCenterArea.setVisible(true);
        sendMessageToManyCenterArea.setVisible(false);
        searchResultArea.setVisible(false);
        userProfileArea.setVisible(false);
        historyArea.setVisible(false);
        yourEventsTableView.setVisible(false);
        addEventArea.setVisible(false);
    }

    private void setCenterToUserProfileArea(){
        friendRequestArea.setVisible(false);
        messageCenterArea.setVisible(false);
        sendMessageToManyCenterArea.setVisible(false);
        searchResultArea.setVisible(false);
        userProfileArea.setVisible(true);
        historyArea.setVisible(false);
        yourEventsTableView.setVisible(false);
        addEventArea.setVisible(false);
    }

    private void setCenterToMessageToManyArea(){
        friendRequestArea.setVisible(false);
        messageCenterArea.setVisible(false);
        sendMessageToManyCenterArea.setVisible(true);
        searchResultArea.setVisible(false);
        userProfileArea.setVisible(false);
        historyArea.setVisible(false);
        yourEventsTableView.setVisible(false);
        addEventArea.setVisible(false);
    }

    private void setCenterToHistoryArea(){
        friendRequestArea.setVisible(false);
        messageCenterArea.setVisible(false);
        sendMessageToManyCenterArea.setVisible(false);
        searchResultArea.setVisible(false);
        userProfileArea.setVisible(false);
        historyArea.setVisible(true);
        yourEventsTableView.setVisible(false);
        addEventArea.setVisible(false);
    }

    private void setCenterToSearchResultArea(){
        friendRequestArea.setVisible(false);
        messageCenterArea.setVisible(false);
        sendMessageToManyCenterArea.setVisible(false);
        searchResultArea.setVisible(true);
        userProfileArea.setVisible(false);
        historyArea.setVisible(false);
        yourEventsTableView.setVisible(false);
        addEventArea.setVisible(false);
    }

    private void setCenterToYourEventsArea(){
        friendRequestArea.setVisible(false);
        messageCenterArea.setVisible(false);
        sendMessageToManyCenterArea.setVisible(false);
        searchResultArea.setVisible(false);
        userProfileArea.setVisible(false);
        historyArea.setVisible(false);
        yourEventsTableView.setVisible(true);
        addEventArea.setVisible(false);
    }
    @FXML
    public void initialize(){

        //setting up alert labels
        addEventErrorLabel.setId("alert-label");
        sendFriendRequestErrorLabel.setId("alert-label");
        historyErrorLabel.setId("alert-label");

        //setting up the written logo
        logoLabel.setStyle("-fx-font-size: 70");

        //setting up the weird circle label
        circleLabel.setId("circle-label");

        //other stuff

        currentSearchedUser = "";
        previousSearchPageButton.setDisable(true);

        userProfilePreviousFriendsPageButton.setDisable(true);

        userProfilePreviousEventPage.setDisable(true);





        setCenterToMessageArea();
        rmsg = null;
        replyLabel.setVisible(false);
        currentConversationPartner = null;
        historyWithFriendsComboBox.setDisable(true);
        messageMoreButton.setDisable(true);
        initLeftBorder();
        initSearch();
        initEventArea();



        ContextMenu receivedFriendshipsContextMenu = new ContextMenu();
        MenuItem acceptRequestButton = new MenuItem("Accept");
        MenuItem rejectRequestButton = new MenuItem("Reject");

        acceptRequestButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    friendRequestService.acceptRequest(receivedFriendRequestListView
                            .getSelectionModel()
                            .getSelectedItem()
                            .getUtilizator()
                            .getId());
                } catch (Exception e) {
                    Alert msg = new Alert(Alert.AlertType.ERROR, e.getMessage());
                    msg.show();
                }
            }
        });

        rejectRequestButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    friendRequestService.rejectRequest(receivedFriendRequestListView
                            .getSelectionModel()
                            .getSelectedItem()
                            .getUtilizator()
                            .getId());
                } catch (SQLException | ClassNotFoundException throwables) {
                    Alert msg = new Alert(Alert.AlertType.ERROR, throwables.getMessage());
                    msg.show();
                }

            }
        });
        receivedFriendshipsContextMenu.getItems().addAll(acceptRequestButton, rejectRequestButton);
        receivedFriendRequestListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(receivedFriendRequestListView.getSelectionModel().isEmpty())
                    return;
                receivedFriendshipsContextMenu.show(receivedFriendRequestListView, Side.LEFT, 0, 23* receivedFriendRequestListView.getSelectionModel().getSelectedIndex());
            }
        });

        sendFriendRequestButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(sendFriendRequestTextField.getText().isEmpty()) {
                    sendFriendRequestErrorLabel.setTextFill(Color.RED);
                    sendFriendRequestErrorLabel.setText("Enter an email first");
                    return;
                }
                try {
                    friendRequestService.sendFriendRequest(networkService.getCredentials(sendFriendRequestTextField.getText()).getId());
                    sendFriendRequestErrorLabel.setTextFill(Color.GREEN);
                    sendFriendRequestErrorLabel.setText("Friend request sent");
                } catch (Exception e) {
                    sendFriendRequestErrorLabel.setTextFill(Color.RED);
                    sendFriendRequestErrorLabel.setText(e.getMessage());
                }
            }
        });

        ContextMenu pendingFriendRequestContextMenu = new ContextMenu();
        MenuItem deleteItem = new MenuItem("Delete");
        deleteItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    friendRequestService.deleteFriendRequest(pendingFriendRequestListView
                    .getSelectionModel()
                    .getSelectedItem()
                    .getUtilizator()
                    .getId(),
                    pendingFriendRequestListView.
                    getSelectionModel()
                    .getSelectedItem()
                    .getData()
                    );
                } catch (SQLException | ClassNotFoundException e ) {
                    Alert msg = new Alert(Alert.AlertType.ERROR, e.getMessage());
                    msg.show();
                }


            }

        });
        pendingFriendRequestContextMenu.getItems().add(deleteItem);
        pendingFriendRequestListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(pendingFriendRequestListView.getSelectionModel().isEmpty())
                    return;
                pendingFriendRequestContextMenu.show(pendingFriendRequestListView, Side.LEFT, 0, 23* pendingFriendRequestListView.getSelectionModel().getSelectedIndex());

            }
        });

        sendMessageButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                List<Long> listUsers = new ArrayList<>();
                listUsers.add(currentConversationPartner.getId());
                replyLabel.setVisible(false);
                try {
                    //socialnetwork.domain.Message rmsg = null;

                    if(rmsg != null)
                        messageService.send(sendMessageTextField.getText(), listUsers, rmsg.getId());
                    else
                        messageService.send(sendMessageTextField.getText(), listUsers, null);
                    List<socialnetwork.domain.Message> list = messageService.conversationWith(currentConversationPartner);
                    messagingArea.clear();

                    list.forEach(x -> {
                        String aux = x.getData().toLocalDate()
                                + " "
                                + x.getData().toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm"))
                                + " ";
                        if (x.getFrom().equals(user.getId()))
                            aux += "Me: ";
                        else
                            aux += (networkService.getCredentialsById(x.getFrom()) + ": ");
                        if(x.getRepliedMessage() != null)
                            aux += "Replying to: " + messageService.getMessageById(x.getRepliedMessage()).getMessage() + "\n";
                        aux = aux
                                + " "
                                + x.getMessage();


                        messagingArea.setText(messagingArea.getText() + "\n" + aux);
                    });
                    sendMessageTextField.clear();
                    rmsg = null;
                    messagingArea.positionCaret(messagingArea.getText().length());

            } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ContextMenu messageContextMenu = new ContextMenu();
        MenuItem replyMenuItem = new MenuItem("Reply");
        MenuItem sendToMoreMenuItem = new MenuItem("Send to more");
        messageContextMenu.getItems().addAll(replyMenuItem, sendToMoreMenuItem);

        replyMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                replyLabel.setVisible(true);

                Popup popup = new Popup();

                VBox vBox = new VBox();
                messageService.conversationWith(currentConversationPartner).forEach(x->{
                    if(x.getData().getDayOfYear() == LocalDateTime.now().getDayOfYear() &&
                        x.getData().getYear() == LocalDateTime.now().getYear()
                    )
                    {
                        Button button = new Button(x.getMessage());
                        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                replyLabel.setText("Reply to: "+ button.getText());
                                sendMessageTextField.setDisable(false);
                                popup.hide();
                                rmsg = x;
                            }
                        });
                        vBox.getChildren().add(button);
                    }
                });
                ScrollPane scrollPane = new ScrollPane(vBox);

                double popupWidth = sendMessageTextField.getWidth();
                double popupHeight = 200;
                scrollPane.setPrefWidth(popupWidth);
                scrollPane.setPrefHeight(popupHeight);
                scrollPane.setMaxHeight(popupHeight);//Adjust max height of the popup here
                scrollPane.setMaxWidth(popupWidth);//Adjust max width of the popup here
                popup.setX(messagingArea.getScene().getWindow().getX() +
                        messagingArea.getScene().getWindow().getWidth()/2 -
                        popupWidth/2 -
                        3
                );
                popup.setY(messagingArea.getScene().getWindow().getY() +
                        messagingArea.getScene().getWindow().getHeight() -
                        popupHeight -
                        9
                );
                popup.setAutoHide(true);
                popup.getContent().add(scrollPane);
                popup.show(loggedLabel.getScene().getWindow());
            }
        });

        sendToMoreMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                messageCenterArea.setVisible(false);
                sendMessageToManyCenterArea.setVisible(true);

                //initialize friend list
                sendMessageToManyFriendListListView.getItems().addAll(friendListModel);


            }
        });

        messageMoreButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                messageContextMenu.show(messageMoreButton,Side.TOP, 0, 0);
            }
        });

        sendMessageToManyAddButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                socialnetwork.domain.Utilizator u = sendMessageToManyFriendListListView.getSelectionModel().getSelectedItem();
                sendMessageToManyChosenFriendsListView.getItems().add(u);
                sendMessageToManyFriendListListView.getItems().remove(u);
            }
        });

        sendMessageToManyRemoveButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                socialnetwork.domain.Utilizator u = sendMessageToManyChosenFriendsListView.getSelectionModel().getSelectedItem();
                sendMessageToManyFriendListListView.getItems().add(u);
                sendMessageToManyChosenFriendsListView.getItems().remove(u);
            }


        });

        sendMessageToManySendButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                List<Long> chosenFriendsList = sendMessageToManyChosenFriendsListView.getItems().stream().map(x->x.getId()).collect(Collectors.toList());

                try {
                    messageService.send(sendMessageToManyTextField.getText(), chosenFriendsList, null);
                    sendMessageToManyCenterArea.setVisible(false);
                    messageCenterArea.setVisible(true);
                    messagingArea.clear();
                    sendMessageTextField.setDisable(true);
                    messageMoreButton.setDisable(true);
                    replyLabel.setVisible(false);
                } catch (Exception e) {
                    Alert msg = new Alert(Alert.AlertType.ERROR, e.getMessage());
                    msg.show();
                }
            }
        });

        sendMessageToManyCloseButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sendMessageToManyCenterArea.setVisible(false);
                messageCenterArea.setVisible(true);
                messagingArea.clear();
                sendMessageTextField.setDisable(true);
                messageMoreButton.setDisable(true);
            }
        });


        historyTypeComboBox.getItems().addAll("All your new friends and messages",
                "All messages with your friend");


        historyTypeComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(historyTypeComboBox.getSelectionModel().getSelectedItem().equals("All messages with your friend")) {
                    historyWithFriendsComboBox.setDisable(false);
                    historyWithFriendsComboBox.getItems().clear();
                    historyWithFriendsComboBox.getItems().addAll(friendListListView.getItems());
                }
                else {
                    historyWithFriendsComboBox.setDisable(true);

                }

            }
        });
        exportButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(startDatePicker.getValue() == null || endDatePicker.getValue() == null || historyTypeComboBox.getSelectionModel().isEmpty()){
                    historyErrorLabel.setTextFill(Color.RED);
                    historyErrorLabel.setText("You have to select a start date, end date and a history type");
                    return;
                }
                String string = "";
                if(historyTypeComboBox.getSelectionModel().getSelectedItem().equals("All messages with your friend") ){
                    if(historyWithFriendsComboBox.getValue() == null){
                        historyErrorLabel.setTextFill(Color.RED);
                        historyErrorLabel.setText("You have to select a friend");
                        return;
                    }
                    List<socialnetwork.domain.Message> messageList = messageService.conversationWithBetweenDates(
                            historyWithFriendsComboBox.getValue(),
                            startDatePicker.getValue(),
                            endDatePicker.getValue()
                    );
                    for (socialnetwork.domain.Message message : messageList) {
                        String aux = message.getData().toLocalDate()
                                + " "
                                + message.getData().toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm"))
                                + " ";
                        if (message.getFrom().equals(user.getId()))
                            aux += "Me: ";
                        else
                            aux += (networkService.getCredentialsById(message.getFrom()) + ": ");
                        if (message.getRepliedMessage() != null)
                            aux += "Replying to: " + messageService.getMessageById(message.getRepliedMessage()).getMessage() + "\n";
                        aux = aux
                                + " "
                                + message.getMessage();


                        string += "\n" + aux;
                    }
                    PDFService pdfService = new PDFService();
                    pdfService.generatePDFBasedOnString(string);
                }
                else if (historyTypeComboBox.getValue().equals("All your new friends and messages")){
                    List<socialnetwork.domain.Utilizator> utilizatorList = networkService.friendshipsByUserDate(user,
                            startDatePicker.getValue(),
                            endDatePicker.getValue()
                    );
                    for(socialnetwork.domain.Utilizator u: networkService.friendshipsFor(user)){
                        List<socialnetwork.domain.Message> messageList = messageService.conversationWithBetweenDates(u,
                                startDatePicker.getValue(),
                                endDatePicker.getValue()
                        );
                        if(messageList.isEmpty())
                            continue;
                        string += "With " + u.getFirstName() + " " + u.getLastName() + ":\n";
                        for(socialnetwork.domain.Message message: messageList)
                            string += message.getMessage() + "\n";
                    }
                    Integer maxim = 0;
                    socialnetwork.domain.Utilizator bff = null;
                    for(socialnetwork.domain.Utilizator u: networkService.getAll() ) {
                        Integer current = messageService.noOfMessagesWith(u.getId());
                        if (maxim < current) {
                            maxim = current;
                            bff = u;
                        }
                    }
                    String bffString;
                   if(bff == null){
                       bffString = "";
                   }
                   else
                   bffString = "Your best friend: " + bff.getFirstName() + " " + bff.getLastName() + " .\n"
                           + "Your conversation is " + maxim + " messages long!";
                    PDFService pdfService = new PDFService();
                    pdfService.generatePDFForNewFriendsAndMessages(utilizatorList, bffString, string);
                    historyErrorLabel.setTextFill(Color.GREEN);
                    historyErrorLabel.setText("Exported");
                }


            }
        });

        initFriendRequestArea();

        historyWithFriendsComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(startDatePicker.getValue() == null ||
                        endDatePicker.getValue() == null ||
                        historyTypeComboBox.getSelectionModel().isEmpty() ||
                        historyWithFriendsComboBox.getSelectionModel().isEmpty()
                ){
                    return;
                }
                List<socialnetwork.domain.Message> messageList = messageService.conversationWithBetweenDates(
                        historyWithFriendsComboBox.getValue(),
                        startDatePicker.getValue(),
                        endDatePicker.getValue()
                );
                historyTextArea.clear();

                messageList.forEach(x -> {
                    String aux = x.getData().toLocalDate()
                            + " "
                            + x.getData().toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm"))
                            + " ";
                    if (x.getFrom().equals(user.getId()))
                        aux += "Me: ";
                    else
                        aux += (networkService.getCredentialsById(x.getFrom()) + ": ");
                    if (x.getRepliedMessage() != null)
                        aux += "Replying to: " + messageService.getMessageById(x.getRepliedMessage()).getMessage() + "\n";
                    aux = aux
                            + " "
                            + x.getMessage();


                    historyTextArea.setText(historyTextArea.getText() + "\n" + aux);
                });
                historyTextArea.positionCaret(historyTextArea.getText().length());

            }
        });

        historyTypeComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(startDatePicker.getValue() == null ||
                        endDatePicker.getValue() == null
                ){
                    return;
                }
                if(historyTypeComboBox.getValue().equals("All messages with your friend")) {
                    historyTextArea.clear();
                    historyWithFriendsComboBox.setDisable(false);
                    historyWithFriendsComboBox.getItems().clear();
                    historyWithFriendsComboBox.getItems().addAll(friendListModel);
                }
                else{
                    historyWithFriendsComboBox.setDisable(true);
                    historyTextArea.clear();

                    List<socialnetwork.domain.Utilizator> utilizatorList = networkService.friendshipsByUserDate(user,
                                startDatePicker.getValue(),
                                endDatePicker.getValue()
                            );
                    historyTextArea.setText("Your new friends in this time period: ");

                    utilizatorList.forEach(x->historyTextArea.setText(historyTextArea.getText() + "\n" + x));
                    historyTextArea.setText(historyTextArea.getText() + "\n Your messages: ");
                    for(socialnetwork.domain.Utilizator u: networkService.friendshipsFor(user)){
                        List<socialnetwork.domain.Message> messageList = messageService.conversationWithBetweenDates(u,
                                startDatePicker.getValue(),
                                endDatePicker.getValue()
                        );
                        if(messageList.isEmpty())
                            continue;
                        historyTextArea.setText(historyTextArea.getText() + "\n" + "With " + u.getFirstName() + " " + u.getLastName());

                        for(socialnetwork.domain.Message message: messageList)
                            historyTextArea.setText(historyTextArea.getText() + "\n" + message.getMessage());

                    }

                }
            }
        });

        startDatePicker.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(endDatePicker.getValue() == null ||
                        historyTypeComboBox.getSelectionModel().isEmpty()){
                    return;
                }

                if(historyTypeComboBox.getValue().equals("All messages with your friend")) {
                    if(startDatePicker.getValue()!= null &&
                            endDatePicker.getValue() != null) {
                        historyWithFriendsComboBox.setDisable(false);
                        historyWithFriendsComboBox.getItems().clear();
                        historyWithFriendsComboBox.getItems().addAll(friendListModel);
                    }
                    if (historyWithFriendsComboBox.getValue() == null)
                        return;

                    List<socialnetwork.domain.Message> messageList = messageService.conversationWithBetweenDates(
                            historyWithFriendsComboBox.getValue(),
                            startDatePicker.getValue(),
                            endDatePicker.getValue()
                    );

                    historyTextArea.clear();

                    messageList.forEach(x -> {
                        String aux = x.getData().toLocalDate()
                                + " "
                                + x.getData().toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm"))
                                + " ";
                        if (x.getFrom().equals(user.getId()))
                            aux += "Me: ";
                        else
                            aux += (networkService.getCredentialsById(x.getFrom()) + ": ");
                        if (x.getRepliedMessage() != null)
                            aux += "Replying to: " + messageService.getMessageById(x.getRepliedMessage()).getMessage() + "\n";
                        aux = aux
                                + " "
                                + x.getMessage();


                        historyTextArea.setText(historyTextArea.getText() + "\n" + aux);
                    });


                }
                else {
                    List<socialnetwork.domain.Utilizator> utilizatorList = networkService.friendshipsByUserDate(user,
                            startDatePicker.getValue(),
                            endDatePicker.getValue()
                    );
                    historyTextArea.setText("Your new friends in this time period: ");

                    utilizatorList.forEach(x->historyTextArea.setText(historyTextArea.getText() + "\n" + x));

                    historyTextArea.setText(historyTextArea.getText() + "\n Your messages: ");
                    for(socialnetwork.domain.Utilizator u: networkService.friendshipsFor(user)){
                        List<socialnetwork.domain.Message> messageList = messageService.conversationWithBetweenDates(u,
                                startDatePicker.getValue(),
                                endDatePicker.getValue()
                        );
                        if(messageList.isEmpty())
                            continue;
                        historyTextArea.setText(historyTextArea.getText() + "\n" + "With " + u.getFirstName() + " " + u.getLastName());

                        for(socialnetwork.domain.Message message: messageList)
                            historyTextArea.setText(historyTextArea.getText() + "\n" + message.getMessage());

                    }
                }
            }
        });

        endDatePicker.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(endDatePicker.getValue() == null ||
                        historyTypeComboBox.getSelectionModel().isEmpty()){
                    return;
                }

                if(historyTypeComboBox.getValue().equals("All messages with your friend")) {
                    if(startDatePicker.getValue()!= null &&
                            endDatePicker.getValue() != null) {
                        historyWithFriendsComboBox.setDisable(false);
                        historyWithFriendsComboBox.getItems().clear();
                        historyWithFriendsComboBox.getItems().addAll(friendListModel);
                    }
                    if (historyWithFriendsComboBox.getValue() == null)
                        return;

                    List<socialnetwork.domain.Message> messageList = messageService.conversationWithBetweenDates(
                            historyWithFriendsComboBox.getValue(),
                            startDatePicker.getValue(),
                            endDatePicker.getValue()
                    );

                    historyTextArea.clear();

                    messageList.forEach(x -> {
                        String aux = x.getData().toLocalDate()
                                + " "
                                + x.getData().toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm"))
                                + " ";
                        if (x.getFrom().equals(user.getId()))
                            aux += "Me: ";
                        else
                            aux += (networkService.getCredentialsById(x.getFrom()) + ": ");
                        if (x.getRepliedMessage() != null)
                            aux += "Replying to: " + messageService.getMessageById(x.getRepliedMessage()).getMessage() + "\n";
                        aux = aux
                                + " "
                                + x.getMessage();
                        historyTextArea.setText(historyTextArea.getText() + "\n" + aux);


                    });
                }
                else {
                    List<socialnetwork.domain.Utilizator> utilizatorList = networkService.friendshipsByUserDate(user,
                            startDatePicker.getValue(),
                            endDatePicker.getValue()
                    );
                    historyTextArea.setText("Your new friends in this time period: ");

                    utilizatorList.forEach(x->historyTextArea.setText(historyTextArea.getText() + "\n" + x));

                    historyTextArea.setText(historyTextArea.getText() + "\n\nYour messages: ");
                    for(socialnetwork.domain.Utilizator u: networkService.friendshipsFor(user)){
                        List<socialnetwork.domain.Message> messageList2 = messageService.conversationWithBetweenDates(u,
                                startDatePicker.getValue(),
                                endDatePicker.getValue()
                        );
                        if(messageList2.isEmpty())
                            continue;
                        historyTextArea.setText(historyTextArea.getText() + "\n" + "With " + u.getFirstName() + " " + u.getLastName());

                        for(socialnetwork.domain.Message message: messageList2)
                            historyTextArea.setText(historyTextArea.getText() + "\n" + message.getMessage());

                    }
                }
            }
        });

    }




    public void initPendingFriendRequests(){
        List<socialnetwork.domain.FriendRequest> f = this.friendRequestService.pendingFriendRequests();
        pendingFriendRequests.clear();
        f.forEach(x->{pendingFriendRequests.add(
                new UserFriendRequestDateDTO(networkService.getCredentialsById(x.getDestination()), x.getDate()));});
        pendingFriendRequestListView.setItems(pendingFriendRequests);


    }

    public void initReceivedFriendRequests(){
        List<socialnetwork.domain.FriendRequest> f = this.friendRequestService.myFriendRequests();
        receivedFriendRequests.clear();
        f.forEach(x->{receivedFriendRequests.add(
                new UserFriendRequestDateDTO(networkService.getCredentialsById(x.getSource()), x.getDate()));});
        receivedFriendRequestListView.setItems(receivedFriendRequests);

    }

    public void initFriendList(){
        List<socialnetwork.domain.Utilizator> f = networkService.getFriendsOnCurrentPage(user);

        friendListModel.clear();

        friendListModel.addAll(f);
        friendListListView.setItems(friendListModel);
    }

    @Override
    public void update() {
        initPendingFriendRequests();
        initReceivedFriendRequests();
        initFriendList();

    }


}
