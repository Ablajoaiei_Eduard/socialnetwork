package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.w3c.dom.Text;
import socialnetwork.domain.Utilizator;
import utils.Crypter;

import java.io.IOException;


public class LoginScreenController {
    socialnetwork.service.NetworkService service;
    @FXML
    javafx.scene.control.TextField emailField;
    @FXML
    PasswordField passwdField;
    @FXML
    Button loginButton;

    @FXML
    Label logoLabel;

    @FXML
    Label loginErrorLabel;



    public void setService(socialnetwork.service.NetworkService service){
        this.service = service;
    }



    @FXML
    public void initialize(){
            logoLabel.setStyle("-fx-font-size: 40");


            loginErrorLabel.setTextFill(Color.GREEN);



    }

    @FXML
    public void handleLogin(){
        try{

            if(emailField.getText().isEmpty())
                throw new Exception();
            socialnetwork.domain.Utilizator u = this.service.getCredentials(emailField.getText());

            if(!service.verifyPassword(u, passwdField.getText())){
                loginErrorLabel.setTextFill(Color.RED);
                loginErrorLabel.setText("Incorrect password");
                return;
            }

            FXMLLoader messageLoader = new FXMLLoader();
            messageLoader.setLocation(getClass().getResource("/mainScreen.fxml"));

            BorderPane root = messageLoader.load();
            MainScreenController mainScreenController = messageLoader.getController();
            mainScreenController.setUser(u);


            mainScreenController.loggedLabel.setText("Logged as "+ u.getFirstName() + " " + u.getLastName());
            Stage stage = new Stage();
            stage.setTitle("Caffeinstagram");
            stage.setScene(new Scene(root, 929, 580));
            stage.getIcons().add(new Image("/images/coffeelogo.png"));
            stage.show();
            Stage stage1 = (Stage) this.emailField.getScene().getWindow();
            stage1.close();



        }
        catch(Exception e){
            loginErrorLabel.setTextFill(Color.RED);
            loginErrorLabel.setText("There is no user with this e-mail");
        }

    }

    @FXML
    public void handleRegister(){
        try {
            loginErrorLabel.setTextFill(Color.GREEN);
            FXMLLoader messageLoader = new FXMLLoader();
            messageLoader.setLocation(getClass().getResource("/registerScreen.fxml"));
            HBox root = messageLoader.load();
            RegisterScreenController registerScreenController = messageLoader.getController();
            registerScreenController.setNetworkService(service);
            Stage stage = new Stage();
            stage.setTitle("Caffeinstagram");
            stage.setScene(new Scene(root, 533, 441));
            stage.getIcons().add(new Image("/images/coffeelogo.png"));
            stage.show();
            Stage stage1 = (Stage) this.emailField.getScene().getWindow();
            stage1.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
