package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

public class Message extends Entity<Long>{
    Long from;
    List<Long> to;
    Long repliedMessage;

    public Long getRepliedMessage() {
        return repliedMessage;
    }

    public void setRepliedMessage(Long repliedMessage) {
        this.repliedMessage = repliedMessage;
    }

    public Message(Long from, List<Long> to, String message, LocalDateTime data) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.data = data;
        this.repliedMessage = null;
        this.setId((long)this.hashCode());
    }

    public Message(Long from, List<Long> to, String message, LocalDateTime data, Long reply) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.data = data;
        this.repliedMessage = reply;
        this.setId((long)this.hashCode());
    }

    public Long getFrom() {
        return from;
    }

    @Override
    public String toString() {
        String result = "";
        if(repliedMessage != null)
            result = result + "REPLIED TO: " + repliedMessage;
        result = result + data.toLocalDate() + " " + data.toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm")) + " " +  message;
        return result;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public List<Long> getTo() {
        return to;
    }

    public void setTo(List<Long> to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    String message;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return from.equals(message1.from) &&
                message.equals(message1.message) &&
                data.equals(message1.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, message, data);
    }

    LocalDateTime data;
}
