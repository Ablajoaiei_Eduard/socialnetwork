package domain;

import jdk.vm.ci.meta.Local;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Eveniment extends socialnetwork.domain.Entity<Long> {
    List<Long> subscribers;
    Long publisher;
    String name;
    LocalDate date;

    public Eveniment(Long publisher, String name, LocalDate date, List<Long> subscribers) {
        this.subscribers = subscribers;
        this.publisher = publisher;
        this.name = name;
        this.date = date;
        this.setId((long) this.hashCode());
        this.subscribers = new ArrayList<>();
    }

    public Eveniment(Long publisher, String name, List<Long> subscribers) {
        this.subscribers = subscribers;
        this.publisher = publisher;
        this.name = name;
        this.setId((long) this.hashCode());
        this.subscribers = new ArrayList<>();
    }

    public Eveniment(Long publisher, String name, LocalDate date){
        this.publisher = publisher;
        this.name = name;
        this.date = date;
        this.setId((long) this.hashCode());
        this.subscribers = new ArrayList<>();
    }

    public Eveniment(Long id, Long publisher, String name, LocalDate date){
        this.setId(id);
        this.publisher = publisher;
        this.name = name;
        this.date = date;
        this.subscribers = new ArrayList<>();
    }

    public List<Long> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<Long> subscribers) {
        this.subscribers = subscribers;
    }

    public Long getPublisher() {
        return publisher;
    }

    public void setPublisher(Long publisher) {
        this.publisher = publisher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Eveniment eveniment = (Eveniment) o;
        return Objects.equals(publisher, eveniment.publisher) &&
                Objects.equals(name, eveniment.name) &&
                Objects.equals(date, eveniment.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(publisher, name, date);
    }

    @Override
    public String toString(){
        return this.name + "     " + this.getDate();
    }

}
