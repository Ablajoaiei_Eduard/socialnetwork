package domain;

import socialnetwork.domain.Utilizator;

import java.time.LocalDateTime;

public class UserFriendRequestDateDTO {
    socialnetwork.domain.Utilizator utilizator;
    LocalDateTime data;

    @Override
    public String toString() {
        return utilizator + ",    " + data ;
    }

    public UserFriendRequestDateDTO(Utilizator utilizator, LocalDateTime data) {
        this.utilizator = utilizator;
        this.data = data;
    }

    public Utilizator getUtilizator() {
        return utilizator;
    }

    public void setUtilizator(Utilizator utilizator) {
        this.utilizator = utilizator;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }
}
