package domain;

import socialnetwork.domain.Utilizator;

import java.time.LocalDateTime;

public class FriendDateDTO {
    socialnetwork.domain.Utilizator u;
    LocalDateTime date;

    public FriendDateDTO(Utilizator u, LocalDateTime date) {
        this.u = u;
        this.date = date;
    }

    public Utilizator getU() {
        return u;
    }

    public void setU(Utilizator u) {
        this.u = u;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
