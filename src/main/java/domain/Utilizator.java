package socialnetwork.domain;



import domain.FriendDateDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Utilizator extends socialnetwork.domain.Entity<Long> {
    private String mail;
    private String firstName;
    private String lastName;
    private List<Utilizator> friends;
    private List<FriendDateDTO> friendsWithDates;
    private String hashedPassword;

    public List<FriendDateDTO> getFriendsWithDates() {
        return friendsWithDates;
    }

    public void setFriendsWithDates(List<FriendDateDTO> friendsWithDates) {
        this.friendsWithDates = friendsWithDates;
    }

    public Utilizator(String mail, String firstName, String lastName) {
        this.mail = mail;
        this.firstName = firstName;
        this.lastName = lastName;
        this.friends = new ArrayList<Utilizator>();
        this.friendsWithDates = new ArrayList<FriendDateDTO>();
        this.setId((long)this.hashCode());

    }

    public Utilizator(String mail, String firstName, String lastName, String hashedPassword) {
        this.mail = mail;
        this.firstName = firstName;
        this.lastName = lastName;
        this.friends = new ArrayList<Utilizator>();
        this.friendsWithDates = new ArrayList<FriendDateDTO>();
        this.setId((long)this.hashCode());
        this.hashedPassword = hashedPassword;

    }

    public void addFriendDate(FriendDateDTO friendDateDTO){
        this.friendsWithDates.add(friendDateDTO);
    }




    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Utilizator> getFriends() {
        return friends;
    }


    /**
     *
     * @param ot- the other user
     * @return null if the users were already friends, ot otherwise
     */
    public Utilizator addPrieten(Utilizator ot){
        //Utilizator aux = new Utilizator(ot.mail, ot.getFirstName(), ot.getLastName());
        if(this.getFriends().contains(ot))
            return null;
        this.getFriends().add(ot);

        return ot;
    }


    /**
     *
     * @param ot - the other user
     * @return null, if the users were not friends in the first place, ot, otherwise
     */
    public Utilizator removePrieten(Utilizator ot){
        if(!this.getFriends().contains(ot))
            return null;
        this.getFriends().remove(ot);
        return ot;
    }


    private String listToString(){
        String rez = "";
        for(Utilizator u: friends)
            rez = rez + u.getLastName() + " " + u.getFirstName() + "\n";
        return rez;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    @Override
    public String toString() {
        return firstName + " " +
                lastName ;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilizator)) return false;
        Utilizator that = (Utilizator) o;
        return getMail().equals(that.getMail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMail());
    }
}