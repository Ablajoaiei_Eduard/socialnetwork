package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.Objects;

public class FriendRequest extends Entity<Tuple<Long,Long>> {
    Long source;
    Long destination;
    LocalDateTime date;

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public FriendRequest(Long id1, Long id2){
        source = id1;
        destination = id2;
        Tuple<Long,Long> tuple = new Tuple<>(id1,id2);
        this.setId(tuple);
        date = LocalDateTime.now();
    }

    public FriendRequest(Long id1, Long id2, LocalDateTime date){
        source = id1;
        destination = id2;
        Tuple<Long,Long> tuple = new Tuple<>(id1,id2);
        this.setId(tuple);
        this.date = date;
    }

    public Long getSource() {
        return source;
    }

    public void setSource(Long source) {
        this.source = source;
    }

    public Long getDestination() {
        return destination;
    }

    public void setDestination(Long destination) {
        this.destination = destination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendRequest that = (FriendRequest) o;
        return source.equals(that.source) &&
                destination.equals(that.destination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, destination);
    }


}
