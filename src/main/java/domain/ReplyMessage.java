package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.List;

public class ReplyMessage extends Message {
    Message message;

    public ReplyMessage(Long from, List<Long> to, String message, LocalDateTime data, Message repliedMessage) {
        super(from, to, message, data);
        this.message = repliedMessage;
    }
}
