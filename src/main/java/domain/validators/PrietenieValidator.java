package socialnetwork.domain.validators;

import socialnetwork.domain.Prietenie;

public class PrietenieValidator implements Validator<Prietenie> {

    /**
     *
     * @param entity - the friendship to be validated
     * @throws ValidationException if the friendship is not valid
     */
    @Override
    public void validate(Prietenie entity) throws ValidationException {
        String errors = "";
        if(entity.getId().getLeft().equals(entity.getId().getRight()))
            errors += "Nu poti fi prieten cu tine insuti.";
        if(!errors.isEmpty())
            throw new ValidationException(errors);
    }
}
