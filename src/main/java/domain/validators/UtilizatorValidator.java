package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;

public class UtilizatorValidator implements socialnetwork.domain.validators.Validator<Utilizator> {

    /**
     *
     * @param entity the user to be validated
     * @throws ValidationException if the user is not valid
     */
    @Override
    public void validate(Utilizator entity) throws ValidationException {

        String errors = "";

        if(entity.getMail().isEmpty())
            errors+= "Mailul nu poate fi vid.\n";

        if(!entity.getMail().contains("@"))
            errors+= "Mailul trebuie sa contina @";


        if(entity.getFirstName().isEmpty() || entity.getLastName().isEmpty())
            errors += "Utilizatorii nu pot avea nume vide.\n";

        if(!entity.getFirstName().chars().allMatch(Character::isLetter) || !entity.getLastName().chars().allMatch(Character::isLetter))
            errors += "Utilizatorii nu pot avea decat litere mari si mici in nume.\n" ;

        if(entity.getFirstName().length() > 30)
            errors += "Prenumele trebuie sa aiba cel mult 30 de caractere.\n" ;

        if(entity.getLastName().length() > 30)
            errors += "Numele trebuie sa aiba cel mult 30 de caractere.\n" ;

        if(!errors.isEmpty())
            throw new ValidationException(errors);
    }
}
