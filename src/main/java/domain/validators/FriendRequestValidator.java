package socialnetwork.domain.validators;


import socialnetwork.domain.FriendRequest;

public class FriendRequestValidator implements Validator<FriendRequest> {
    @Override
    public void validate(FriendRequest entity) throws ValidationException {
        if(entity.getSource() == null || entity.getDestination() == null)
            throw new ValidationException("Nu pot fi null.");
    }
}
