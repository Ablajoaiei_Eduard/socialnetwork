package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.Objects;


public class Prietenie extends Entity<Tuple<Long,Long>> {

    LocalDateTime date;

    public Prietenie(Long id1, Long id2) {
        Tuple<Long, Long> tuple = new Tuple<>(id1, id2);
        this.setId(tuple);

        this.date = LocalDateTime.now();


    }

    public Prietenie(Long id1, Long id2, LocalDateTime date) {
        Tuple<Long, Long> tuple = new Tuple<>(id1, id2);
        this.setId(tuple);

        this.date = date;

    }

    @Override
    public boolean equals(Object ot){
        if(this == ot)
            return true;
        if(ot == null)
            return false;
        if(ot.getClass() != this.getClass())
            return false;
        Prietenie aux = (Prietenie) ot;
        return Objects.equals(this.getId().getLeft(),aux.getId().getLeft()) &&
                Objects.equals(this.getId().getRight(), aux.getId().getRight());
    }

    @Override
    public int hashCode(){
        return Objects.hash(this.getId().getLeft(), this.getId().getRight());
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
