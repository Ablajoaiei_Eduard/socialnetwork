package domain;

import socialnetwork.domain.Utilizator;

public class FriendNoMessages {
    socialnetwork.domain.Utilizator u;
    Integer noMessages;

    public FriendNoMessages(Utilizator u, Integer noMessages) {
        this.u = u;
        this.noMessages = noMessages;
    }

    public Utilizator getU() {
        return u;
    }

    public void setU(Utilizator u) {
        this.u = u;
    }

    public Integer getNoMessages() {
        return noMessages;
    }

    public void setNoMessages(Integer noMessages) {
        this.noMessages = noMessages;
    }
}
