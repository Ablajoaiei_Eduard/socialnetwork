package observer;

import events.Event;

public interface Observable  {
    void addObserver(Observer o);
    void removeObserver(Observer o);
    void notifyObservers();
}
