package events;

public class FriendshipsChangedEvent implements Event{
    EventType type;
    socialnetwork.domain.Prietenie data;
    public FriendshipsChangedEvent(EventType type, socialnetwork.domain.Prietenie prietenie){
        this.type = type;
        this.data = prietenie;
    }
}
