package events;

public class FriendRequestsChangedEvent implements Event{
    EventType type;
    socialnetwork.domain.FriendRequest data;

    public FriendRequestsChangedEvent(EventType type, socialnetwork.domain.FriendRequest data) {
        this.type = type;
        this.data = data;
    }
}
